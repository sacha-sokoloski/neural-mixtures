{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE UndecidableInstances,Arrows,BangPatterns #-}

module Decoders


--- Imports ---


-- Package --

import Goal.Core
import Goal.Geometry
import Goal.Probability

import qualified Goal.Core.Vector.Storable as S
import qualified Goal.Core.Vector.Generic as G

import Core

import qualified Data.List as L
import qualified Data.Map.Strict as M

import Data.Complex
import Data.Tuple
import Control.Concurrent.Async


--- Decoders ---


parallelTrainDecoder
    :: forall n f m c i . (KnownNat n, Generative c i, SamplePoint i ~ Double
                        , Propagate Natural f (Categorical m) (Neurons n))
    => Int -- ^ Parallel Trains
    -> c # i -- ^ Initialization
    -> Double -- ^ Learning Rate
    -> Int -- ^ Number of Epochs
    -> Sample (Categorical m,Neurons n) -- ^ Training data
    -> Sample (Categorical m,Neurons n) -- ^ Test data
    -> Sample (Categorical m,Neurons n) -- ^ Validation data
    -> IO (Natural # f (Categorical m) (Neurons n),[Double])
parallelTrainDecoder npr ins eps nepchs txzs exzs vxzs = do
    dcdascnts <- replicateConcurrently npr
        $ trainDecoder ins eps nepchs txzs exzs vxzs
    let (dcds,ascnts) = unzip dcdascnts
        dcd = maximumBy (comparing (conditionalLogLikelihood exzs)) dcds
        ascnt = average <$> L.transpose ascnts
    return (dcd,ascnt)


--trainDecoder
--    :: forall n f m c i . (KnownNat n, Generative c i, SamplePoint i ~ Double
--                        , Propagate Mean Natural f (Categorical m) (Neurons n))
--    => c # i -- ^ Initialization
--    -> Double -- ^ Learning Rate
--    -> Int -- ^ Number of Epochs
--    -> Sample (Categorical m,Neurons n) -- ^ Training data
--    -> Sample (Categorical m,Neurons n) -- ^ Test data
--    -> Sample (Categorical m,Neurons n) -- ^ Validation data
--    -> IO (Natural #> f (Categorical m) (Neurons n),[Double])
--trainDecoder ins eps nepchs txzs exzs vxzs = do
--
--    dcd0 :: Natural #> f (Categorical m) (Neurons n) <- realize $ initialize ins
--
--    let llgrd = conditionalLogLikelihoodDifferential txzs
--        dcds = take nepchs
--            $ vanillaGradientSequence llgrd eps defaultAdamPursuit dcd0
--        --lls = conditionalLogLikelihood exzs <$> dcds
--        --mxll = maximum lls
--        --lli = fromJust $ L.elemIndex mxll lls
--        (idx,dcd) = maximumBy (comparing (conditionalLogLikelihood exzs . snd))
--            $ zip [0..] dcds
--        ascnt = conditionalLogLikelihood vxzs <$> dcds
--
--    putStrLn $ concat ["Maximum (Index,Log-Likelihood): ", show (idx,ascnt !! idx)]
--    return $!! (dcd,ascnt)

trainDecoder
    :: forall n f m c i . (KnownNat n, Generative c i, SamplePoint i ~ Double
                        , Propagate Natural f (Categorical m) (Neurons n))
    => c # i -- ^ Initialization
    -> Double -- ^ Learning Rate
    -> Int -- ^ Number of Epochs
    -> Sample (Categorical m,Neurons n) -- ^ Training data
    -> Sample (Categorical m,Neurons n) -- ^ Test data
    -> Sample (Categorical m,Neurons n) -- ^ Validation data
    -> IO (Natural # f (Categorical m) (Neurons n),[Double])
trainDecoder ins eps nepchs txzs exzs vxzs = do

    dcd0 :: Natural # f (Categorical m) (Neurons n) <- realize $ initialize ins
    let nbtch = 500
        mlt = fromIntegral (length txzs) / fromIntegral nbtch :: Double
        nstps = round $ fromIntegral nepchs * mlt

    let ell0 = conditionalLogLikelihood exzs dcd0
        --chn :: Chain (Int,Natural #> f (Categorical m) (Neurons n)
        chn = chainCircuit (0 :: Int,dcd0,ell0,[]) . accumulateCircuit (0,dcd0)
            $ proc ((mxk,mxdcd,mxell,ascnt),(k,dcd)) -> do
            txzs' <- minibatcher nbtch txzs -< ()
            let !ddcd = vanillaGradient $ conditionalLogLikelihoodDifferential txzs' dcd
            !dcd' <- gradientCircuit eps defaultAdamPursuit -< (dcd, ddcd)
            let !ell' = conditionalLogLikelihood exzs dcd'
                !vll' = conditionalLogLikelihood vxzs dcd'
                !(!mxk',!mxdcd',!mxell') = if ell' > mxell
                            then (k+1,dcd',ell')
                            else (mxk,mxdcd,mxell)
            returnA -< ( (mxk',mxdcd',mxell', vll' : ascnt)
                       , (k+1,dcd') )

        --lls = conditionalLogLikelihood exzs <$> dcds
        --mxll = maximum lls
        --lli = fromJust $ L.elemIndex mxll lls

    (idx1,dcd1,mxll1,ascnt1) <- realize $ iterateChain nstps chn
        --ascnt = conditionalLogLikelihood vxzs <$> dcds
    let rto = roundSD 3 $ fromIntegral idx1 / mlt

    putStrLn $ "Maximum (Epoch,Log-Likelihood): " ++ show (rto,mxll1)
    return $!! (dcd1, reverse ascnt1)

