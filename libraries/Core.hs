{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE
    GADTs,
    ScopedTypeVariables,
    DataKinds,
    DeriveGeneric,
    FlexibleContexts,
    TypeApplications,
    TypeOperators
    #-}

module Core
    (
    -- * Types
      Neurons
    , CoMNeurons
    , Response
    -- * Variables
    , analysisPath
    , dataPath
    , plotPath
    , parametersPath
    -- * IO
    , neuralDataPath
    , writeNeuralData
    , writeRawNeuralData
    , readNeuralData
    , readLatentVariableModel
    -- * Data Processing
    , strengthenNeuralData
    -- * Training
    , epsilonRange
    , isBoundPoint
    -- * Util
    , linearFisherize
    , linearFisherInformation
    , empiricalLinearFisherInformations
    , toCategorical
    , posteriorPointComparison
    , rSquared'
    -- * CLI
    , ExperimentOpts (ExperimentOpts)
    , experimentOpts
    ) where


--- Imports ---


-- Goal --

import Goal.Core
import Goal.Geometry
import Goal.Probability

import qualified Goal.Core.Vector.Storable as S

import qualified Data.List as L
import qualified Data.Map as M

--- Synonyms ---


type Neurons n = Replicated n Poisson
type CoMNeurons n = Replicated n CoMPoisson
type Response n = SamplePoint (Neurons n)


--- Variables ---


pathRoot :: String -> String
pathRoot = ("./" ++)

analysisPathRoot :: String
analysisPathRoot = pathRoot "experiments"

dataPathRoot :: String
dataPathRoot = pathRoot "data-files"

plotPath :: String
plotPath = pathRoot "plot-files"

analysisPath :: String -> String -> [String] -> String
analysisPath expmnt dst sbdrs =
    let sbdrs' = ('/':) <$> expmnt:dst:sbdrs
     in concat $ analysisPathRoot:sbdrs'

dataPath :: String -> String
dataPath expmnt = concat [dataPathRoot, "/", expmnt]

parametersPath :: String -> FilePath
parametersPath ldpth = ldpth ++ "/parameters.dat"


--- IO ---

neuralDataPath :: String -> String -> FilePath
neuralDataPath expmnt dst = analysisPath expmnt dst [] ++ "/dataset"

writeNeuralData
    :: (Show x, Eq x, ToField x)
    => String -- ^ Experiment
    -> String -- ^ Dataset
    -> [(Response k, x)]  -- ^ aData
    -> IO ()
writeNeuralData expmnt dst zxs = do
    let (zs,xs) = unzip zxs
        zs' = S.toList <$> zs
    writeRawNeuralData expmnt dst $ zip zs' xs

writeRawNeuralData
    :: ToField x
    => String -- ^ Experiment
    -> String -- ^ Dataset
    -> [([Int], x)]  -- ^ Data
    -> IO ()
writeRawNeuralData expmnt dst zxs = do
    createDirectoryIfMissing True $ analysisPath expmnt dst []
    let anlpth = analysisPath expmnt dst []
        (zs,xs) = unzip zxs
    goalExport anlpth "stimuli" $ Only <$> xs
    goalExport anlpth "responses" zs

readNeuralData
    :: String -- ^ Experiment
    -> String -- ^ Dataset
    -> IO (NatNumber,NatNumber,[([Int],String)]) -- (#neurons, #data-classes,data)
readNeuralData expmnt dst = do
    lrxs <- goalImport $ analysisPath expmnt dst ["stimuli"]
    lrzs <- goalImport $ analysisPath expmnt dst ["responses"]
    let xs :: [Only String]
        xs = case lrxs of
            Left err -> error $ "Can't parse stimuli.csv, Error: " ++ err
            Right xs0 -> xs0
    let zs = case lrzs of
            Left err -> error $ "Can't parse responses.csv, Error: " ++ err
            Right zs0 -> zs0
    let lns = L.nub $ length <$> zs
        dn = if length lns /= 1
                then error "Bad responses.csv, not all responses have same length"
                else head lns
        dk = length $ L.nub xs
    when (length xs /= length zs) $ error "Error: Unequal number of trials in stimuli.csv and responses.csv"
    return (fromIntegral dn, fromIntegral dk, zip zs $ fromOnly <$> xs)

readLatentVariableModel
    :: String -- ^ Load Path
    -> IO (NatNumber,NatNumber,[Double])
readLatentVariableModel ldpth =
    let anlpth = parametersPath ldpth
     in read <$> readFile anlpth


--- Data Processing ---


strengthenNeuralData :: KnownNat n => [([Int], s)] -> [(Response n, s)]
strengthenNeuralData xss =
    let (ks,ss) = unzip xss
     in zip (fromJust . S.fromList <$> ks) ss


--- Training ---


epsilonRange :: Double -> Double -> Int -> [Double]
epsilonRange mxeps mneps nepchs =
    exp <$> range (log mxeps) (log mneps) nepchs

isBoundPoint :: Manifold m => c # m  -> Bool
isBoundPoint p =
    let crds = listCoordinates p
     in and [ not $ isNaN crd || isInfinite crd | crd <- crds ]


--- Util ---

linearFisherize
    :: KnownNat n
    => (Double, Double)
    -> (Source # MultivariateNormal n, Source # MultivariateNormal n)
    -> (Double, Double, Double)
linearFisherize (lwx,upx) (lwnrm,upnrm) =
  let (lwmn,lwcvr) = splitMultivariateNormal lwnrm
      (upmn,upcvr) = splitMultivariateNormal upnrm
      icvr = S.pseudoInverse . S.withMatrix (S.scale 0.5) $ lwcvr + upcvr
      ds = upx-lwx
      df = S.scale (1/ds) (lwmn - upmn)
      nv = S.dotProduct df $ S.matrixVectorMultiply icvr df
   in (ds,(upx + lwx)/2, nv)

empiricalCoarseLinearFisherInformations0
    :: KnownNat n
    => (Double,Double) -- Left
    -> ([Response n],[Response n]) -- Right
    -> (Double,Double,Double) -- location/Naive/Corrected
empiricalCoarseLinearFisherInformations0 (lwx,upx) (lwzs,upzs) =
    let lwzs' = S.map fromIntegral <$> lwzs
        upzs' = S.map fromIntegral <$> upzs
        [lwnrm,upnrm] =
            [ transition $ averageSufficientStatistic zs' | zs' <- [lwzs',upzs'] ]
        (ds,mux,nv) = linearFisherize (lwx,upx) (lwnrm,upnrm)
        ntrls = fromIntegral $ length lwzs
        k = fromIntegral . S.length $ head lwzs
        crc = nv*(2*ntrls - 2 - k - 1)/(2*ntrls - 2) - 2*k/(ntrls*square ds)
     in (mux, nv, crc)

linearFisherInformation
    :: KnownNat n
    => M.Map Double (Source # MultivariateNormal n)
    -> [(Double,Double)] -- location/value
linearFisherInformation xnrms =
    let (xs,nrms) = unzip $ M.toAscList xnrms
        lwupxs = zip xs $ tail xs
        lwupnrms = zip nrms $ tail nrms
        (_,lcs,vls) = unzip3 $ zipWith linearFisherize lwupxs lwupnrms
     in zip lcs vls


empiricalLinearFisherInformations
    :: KnownNat k
    => M.Map Double [Response k]
    -> [(Double,Double,Double)] -- location/Naive/Corrected
empiricalLinearFisherInformations xzmp =
    let (xs,zss) = unzip $ M.toAscList xzmp
        lwupxs = zip xs $ tail xs
        lwupzss = zip zss $ tail zss
     in zipWith empiricalCoarseLinearFisherInformations0 lwupxs lwupzss


toCategorical
    :: forall m . KnownNat m
    => [Double]
    -> [Double]
    -> Mean # Categorical m
toCategorical prrs dns0s =
    let dns1s = zipWith (*) prrs dns0s
        dnss = (/ sum dns1s) <$> dns1s
     in Point . fromJust . S.fromList $ tail dnss

posteriorPointComparison
    :: forall m . KnownNat m
    => [(Int,Mean # Categorical m)]
    -> Double
posteriorPointComparison truxmxs =
    average [ log $ density mx trux | (trux,mx) <- truxmxs ]

rSquared'
    :: [(Double,Double)] -- ^ Dependent variable observations
    -> Double -- ^ R-squared
rSquared' yyhts =
    let (ys,yhts) = unzip yyhts
        ybr = average ys
        ssres = sum $ map square (zipWith (-) ys yhts)
        sstot = sum $ map (square . subtract ybr) ys
     in 1 - (ssres/sstot)



--- CLI ---


data ExperimentOpts = ExperimentOpts String String

experimentOpts :: Parser ExperimentOpts
experimentOpts = ExperimentOpts
    <$> strArgument
        ( help "Which data collection to analyze"
        <> metavar "EXPERIMENT" )
    <*> strArgument
        ( help "Which dataset to plot"
        <> metavar "DATASET" )
