{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE
    Arrows,
    TypeApplications,
    UndecidableInstances,
    DeriveGeneric,
    TupleSections,
    BangPatterns,
    OverloadedStrings
    #-}

-- | Population codes and exponential families.
module NeuralMixtures
    (
    -- * Types
      IPCM
    , CBMixture
    , CBCM
    -- * IO
    , writeCM
    -- * Construction
    , joinIPCM
    , splitIPCM
    , joinCBMixture
    , splitCBMixture
    , joinCBCM
    , splitCBCM
    , normalizePoissonWeights
    , normalizeCoMBasedWeights
    -- * Learning
    , fitIPCM
    , stochasticFitIPCM
    , fitCBCM
    , fitMaximalCBCM
    -- * Statistics
    , poissonMixtureCovariance
    , comMixtureCovariance
    , comMixtureSkewnesses
    , comMixtureExcessKurtosises
    , poissonMixtureSkewnesses
    , poissonMixtureExcessKurtosises
    -- * Analysis
    , subsampleNeuralData
    , poissonFisherInformation
    , comFisherInformation
    ) where


--- Imports ---


-- Goal --

import Goal.Core
import Goal.Geometry
import Goal.Probability
import Goal.Graphical

import Core

import qualified Goal.Core.Vector.Storable as S

import qualified Data.List as L
import qualified Data.Map.Strict as M
import Control.Parallel.Strategies


--- Types ---


type IPCM n k x = Affine Tensor (Neurons n) (Mixture (Neurons n) k) x

type CBMixture n k = AffineMixture (Neurons n) (CoMNeurons n) k
type CBCM n k x = Affine Tensor (Neurons n) (CBMixture n k) x


--- Construction ---


joinIPCM
    :: (KnownNat k, KnownNat n, Manifold x)
    => Natural # Neurons n <* x -- ^ population likelihood
    -> S.Vector k (Natural # Neurons n) -- ^ modulations
    -> Natural # Categorical k -- ^ weights
    -> Natural # IPCM n k x -- ^ mixture likelihood
joinIPCM fnx nns nk =
    let (nn0,nnx) = split fnx
        mxmdl = joinNaturalMixture (S.cons nn0 $ S.map (+ nn0) nns) nk
     in join mxmdl nnx

splitIPCM
    :: (KnownNat k, KnownNat n, Manifold x)
    => Natural # IPCM n k x
    -> ( Natural # Neurons n <* x
       , S.Vector k (Natural # Neurons n)
       , Natural # Categorical k )
splitIPCM mlkl =
    let (mxmdl,nnx) = split mlkl
        (nns',nk) = splitNaturalMixture mxmdl
        (nn00,nns) = S.splitAt nns'
        nn0 = S.head nn00
     in (join nn0 nnx, S.map (subtract nn0) nns,nk)

joinCBCM
    :: (KnownNat k, KnownNat n, Manifold x)
    => Natural # Replicated n CoMShape
    -> Natural # IPCM n k x -- ^ mixture likelihood
    -> Natural # CBCM n k x -- ^ mixture likelihood
joinCBCM shps lkl =
    let (mxmdl,nnx) = split lkl
     in join (joinCBMixture shps mxmdl) nnx

splitCBCM
    :: (KnownNat k, KnownNat n, Manifold x)
    => Natural # CBCM n k x -- ^ mixture likelihood
    -> (Natural # Replicated n CoMShape, Natural # IPCM n k x)
splitCBCM lkl' =
    let (mxmdl',nnx) = split lkl'
        (shps,mxmdl) = splitCBMixture mxmdl'
     in (shps,join mxmdl nnx)

mxshp :: Double
mxshp = -0.02

rectifyMinimalCBCM
    :: (Manifold x, KnownNat k, KnownNat n)
    => Natural # CBCM n k x
    -> Natural # CBCM n k x
rectifyMinimalCBCM ccpm =
    let (shps,cpm) = splitCBCM ccpm
        shps' = Point . S.map (min mxshp) $ coordinates shps
     in joinCBCM shps' cpm

rectifyMaximalCBCM
    :: (KnownNat n, KnownNat k, KnownNat s)
    => Natural # CBMixture n k <* Categorical s
    -> Natural # CBMixture n k <* Categorical s
rectifyMaximalCBCM ccpm =
    joinMaximalCM . S.map shpr $ splitMaximalCM ccpm
    where shpr mxmdl =
            let (shps,mxmdl0) = splitCBMixture mxmdl
                shps' = Point . S.map (min mxshp) $ coordinates shps
             in joinCBMixture shps' mxmdl0

joinCBMixture
    :: (KnownNat n, KnownNat k)
    => c # Replicated n CoMShape
    -> c # Mixture (Neurons n) k
    -> c # AffineMixture (Neurons n) (CoMNeurons n) k
joinCBMixture shps mxmdl =
    let (pstr,lcs) = split $ transposeHarmonium mxmdl
        cms = joinReplicated
            $ S.zipWith join (splitReplicated lcs) (splitReplicated shps)
     in transposeHarmonium $ join pstr cms

splitCBMixture
    :: (KnownNat n, KnownNat k)
    => c # AffineMixture (Neurons n) (CoMNeurons n) k
    -> (c # Replicated n CoMShape, c # Mixture (Neurons n) k)
splitCBMixture cmxmdl =
    let (pstr,cms) = split $ transposeHarmonium cmxmdl
        lcs = mapReplicated (fst . split) cms
        shps = mapReplicated (snd . split) cms
     in (joinReplicated shps, transposeHarmonium . join pstr $ joinReplicated lcs)


--- IO ---


writeCM
    :: forall f k n y x . (KnownNat k, KnownNat n, Manifold x)
    => String -- ^ Load Path
    -> Natural # f (AffineMixture (Neurons n) y k) x -- ^ Mixture Likelihood
    -> IO ()
writeCM ldpth lkl = do
    createDirectoryIfMissing True ldpth
    let n = natValInt (Proxy @ n)
        k = natValInt (Proxy @ k)
        flpth = parametersPath ldpth
    writeFile flpth $ show (n,k,listCoordinates lkl)


--- Conjugation ---


-- | Stimulus Dependent Noise Correlations, ordered by preferred stimulus.
poissonMixtureCovariance
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # Mixture (Neurons k) n -- ^ Mixture Encoder
    -> Source # MultivariateNormal k -- ^ Mean Parameter Correlations
poissonMixtureCovariance mxmdl =
    let (ngnss, nwghts) = splitNaturalMixture mxmdl
        wghts = S.toList $ categoricalWeights nwghts
        gnss = toMean <$> S.toList ngnss
        mgns = weightedAverage $ zip (realToFrac <$> wghts) gnss
        mgns2 :: Mean # Tensor (Neurons k) (Neurons k)
        mgns2 = mgns >.< mgns
        gnss0 = coordinates <$> gnss
        mmgns = fromMatrix . S.weightedAverageOuterProduct $ zip3 wghts gnss0 gnss0
        cvgns = mmgns - mgns2
        cvnrns = cvgns + (fromMatrix . S.diagonalMatrix $ coordinates mgns)
     in joinMultivariateNormal (coordinates mgns) (toMatrix cvnrns)

-- | Stimulus Dependent Noise Correlations, ordered by preferred stimulus.
comMixtureCovariance
    :: ( KnownNat k, KnownNat n )
    => Natural # CBMixture n k -- ^ Mixture Encoder
    -> Source # MultivariateNormal n -- ^ Mean Parameter Correlations
comMixtureCovariance nhrmc =
    let mhrm = snd . splitCBMixture $ toMean nhrmc
        (mn0,_,_) = splitHarmonium mhrm
        (mns0,mk) = splitMeanMixture mhrm
        wghts = S.toList $ categoricalWeights mk
        mn = coordinates mn0
        mns = coordinates <$> S.toList mns0
        mn2 = S.weightedAverageOuterProduct $ zip3 wghts mns mns
        --mn2 = mn20 - S.diagonalMatrix (S.takeDiagonal mn20)
        vrs = comMixtureAverageVariances nhrmc
        cvr = S.diagonalMatrix vrs + mn2 - S.outerProduct mn mn
     in joinMultivariateNormal mn cvr

comMixtureAverageVariances
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # CBMixture k n -- ^ Mixture Encoder
    -> S.Vector k Double -- ^ Variances
comMixtureAverageVariances nhrmc =
    let mk = snd . split $ toMean nhrmc
        wghts = S.toList $ categoricalWeights mk
        ncns = fst (split nhrmc) >$>* pointSampleSpace mk
     in weightedAverage . zip (realToFrac <$> wghts) $ mapReplicated (comPoissonVariance 1e-16) <$> ncns

comPoissonVariance :: Double -> Natural # CoMPoisson -> Double
comPoissonVariance eps np =
    let moments k = S.fromTuple (fromIntegral k, square $ fromIntegral k)
        (m1,m2) = S.toPair $ comPoissonExpectations eps moments np
     in m2 - square m1

comMixtureSkewnesses
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # CBMixture n k -- ^ Mixture Encoder
    -> S.Vector n Double -- ^ Variances
comMixtureSkewnesses nhrmc =
    let (mu,cvr) = splitMultivariateNormal $ comMixtureCovariance nhrmc
        sds = sqrt $ S.takeDiagonal cvr
        mk = snd . split $ toMean nhrmc
        wghts = S.toList $ categoricalWeights mk
        ncns = fst (split nhrmc) >$>* pointSampleSpace mk
     in weightedAverage . zip (realToFrac <$> wghts)
         $ S.zipWith3 (comPoissonSkewness 1e-16) mu sds . splitReplicated <$> ncns

comPoissonSkewness :: Double -> Double -> Double -> Natural # CoMPoisson -> Double
comPoissonSkewness eps mu sd np =
    let moment k = S.singleton $ ((fromIntegral k - mu) / sd) ^ (3 :: Int)
     -- in (/ (mu**(-1/2))) . S.head $ comPoissonExpectations eps moment np
     in S.head $ comPoissonExpectations eps moment np

poissonMixtureSkewnesses
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # Mixture (Neurons n) k -- ^ Mixture Encoder
    -> S.Vector n Double -- ^ Variances
poissonMixtureSkewnesses nhrmc =
    let mk = snd . split $ toMean nhrmc
        wghts = S.toList $ categoricalWeights mk
        ps = fst (split nhrmc) >$>* pointSampleSpace mk
     in weightedAverage
         . zip (realToFrac <$> wghts) $ S.map (**(-1/2)) . coordinates . toMean <$> ps
     --in S.zipWith (\mu skw -> skw / (mu**(-1/2))) mus . weightedAverage
     --    . zip (realToFrac <$> wghts) $ S.map (**(-1/2)) . coordinates . toMean <$> ps

comMixtureExcessKurtosises
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # CBMixture k n -- ^ Mixture Encoder
    -> S.Vector k Double -- ^ Variances
comMixtureExcessKurtosises nhrmc =
    let (mu,cvr) = splitMultivariateNormal $ comMixtureCovariance nhrmc
        sds = sqrt $ S.takeDiagonal cvr
        mk = snd . split $ toMean nhrmc
        wghts = S.toList $ categoricalWeights mk
        ncns = fst (split nhrmc) >$>* pointSampleSpace mk
     in weightedAverage . zip (realToFrac <$> wghts)
         $ S.zipWith3 (comPoissonExcessKurtosis 1e-16) mu sds . splitReplicated <$> ncns

comPoissonExcessKurtosis :: Double -> Double -> Double -> Natural # CoMPoisson -> Double
comPoissonExcessKurtosis eps mu sd np =
    let moment k = S.singleton $ ((fromIntegral k - mu) / sd) ^ (4 :: Int)
     -- in (/ recip mu) . subtract 3 . S.head $ comPoissonExpectations eps moment np
     in subtract 3 . S.head $ comPoissonExpectations eps moment np

poissonMixtureExcessKurtosises
    :: forall k n . ( KnownNat k, KnownNat n )
    => Natural # Mixture (Neurons n) k -- ^ Mixture Encoder
    -> S.Vector n Double -- ^ Variances
poissonMixtureExcessKurtosises nhrmc =
    let mk = snd . split $ toMean nhrmc
        wghts = S.toList $ categoricalWeights mk
        ps = fst (split nhrmc) >$>* pointSampleSpace mk
     in weightedAverage
         . zip (realToFrac <$> wghts) $ S.map recip . coordinates . toMean <$> ps
     --in S.zipWith (\mu krt -> krt / recip mu) mus . weightedAverage
     --    . zip (realToFrac <$> wghts) $ S.map recip . coordinates . toMean <$> ps


subsampleNeuralData
    :: (KnownNat n, KnownNat n')
    => S.Vector n' Int
    -> [(Response n, x)]
    -> [(Response n', x)]
subsampleNeuralData idxs zxs =
    let (zs,xs) = unzip zxs
        zs' = flip S.backpermute idxs <$> zs
     in zip zs' xs



--- Analysis ---


--- Training ---


stochasticFitIPCM
    :: ( KnownNat k, KnownNat n, ExponentialFamily x, Ord (SamplePoint x), NFData a
       , Show a, Propagate Natural f (Mixture (Neurons n) k) x )
    => [Double]
    -> Int
    -> Int
    -> SampleMap (Neurons n) x -- ^ Training Data
    -> (Natural # f (Mixture (Neurons n) k) x -> a)
    -> Natural # f (Mixture (Neurons n) k) x -- ^ Initial Model
    -> IO (Natural # f (Mixture (Neurons n) k) x, [a]) -- ^ CE Descent
stochasticFitIPCM epss nstps nbtch tzxmp validator mlkl0 = do
    let crc = accumulateFunction mlkl0
            $ stochasticIPCMEpoch validator nstps nbtch tzxmp
    (mlkls,vls) <- unzip <$> streamCircuit crc (zip [1..] epss)
    return (last mlkls, vls)

fitIPCM
    :: ( KnownNat k, KnownNat n, ExponentialFamily x, Ord (SamplePoint x), NFData a
       , Propagate Natural f (Mixture (Neurons k) n) x )
    => [Double]
    -> Int
    -> SampleMap (Neurons k) x -- ^ Training Data
    -> (Natural # f (Mixture (Neurons k) n) x -> a)
    -> Natural # f (Mixture (Neurons k) n) x -- ^ Initial Model
    -> (Natural # f (Mixture (Neurons k) n) x, [a]) -- ^ CE Descent
fitIPCM epss nstps tzxmp validator mlkl0 =
    let (mlkls,vls) = unzip $ L.scanl' scanner (mlkl0, validator mlkl0) epss
     in (last mlkls, vls)
    where
        scanner (mlkl,_) eps =
            let mlkl' = poissonCMEpoch nstps tzxmp mlkl eps
             in (mlkl',) $!! validator mlkl'

poissonCMEpoch
    :: ( KnownNat k, KnownNat m, ExponentialFamily x, Ord (SamplePoint x)
      , Propagate Natural f (Mixture (Neurons k) m) x )
    => Int -- ^ Number of iterations
    -> SampleMap (Neurons k) x -- ^ Training Data
    -> Natural # f (Mixture (Neurons k) m) x -- ^ Initial Likelihood
    -> Double -- ^ Learning Rate
    -> Natural # f (Mixture (Neurons k) m) x -- ^ EM Ascent
poissonCMEpoch nstps xzmp mlkl0 eps =
    let xs = M.keys xzmp
        mxs = sufficientStatistic <$> xs
        zss = M.elems xzmp
        mhrms = [ expectationStep zs nhrmht | (zs,nhrmht) <- zip zss $ mlkl0 >$> mxs ]
        diff mlkl =
            let dfs = zipWith relativeEntropyDifferential mhrms nhrmhts
            --let dfs = parMap rdeepseq (uncurry relativeEntropyDifferential)
            --        $ zip mhrms nhrmhts
                (f',nhrmhts) = propagate dfs mxs mlkl
             in f'
        chn = chainCircuit mlkl0 $ proc mlkl -> do
            let dmlkl = vanillaGradient $ diff mlkl
            mlkl' <- gradientCircuit (-eps) defaultAdamPursuit -< (mlkl, dmlkl)
            returnA -< Point . S.map (min 1e12) $ coordinates mlkl'
        mlkls = runIdentity $ streamChain nstps chn
     in last . takeWhile isBoundPoint $ take nstps mlkls

stochasticIPCMEpoch
    :: ( KnownNat k, KnownNat n, ExponentialFamily x, Ord (SamplePoint x)
       , NFData a, Show a, Propagate Natural f (Mixture (Neurons n) k) x )
    => (Natural # f (Mixture (Neurons n) k) x -> a)
    -> Int -- ^ Number of iterations
    -> Int -- ^ Batch size
    -> SampleMap (Neurons n) x -- ^ Training Data
    -> (Int,Double) -- ^ Epoch/Learning Rate
    -> Natural # f (Mixture (Neurons n) k) x -- ^ Initial Likelihood
    -> IO ( (Natural # f (Mixture (Neurons n) k) x, a)
          , Natural # f (Mixture (Neurons n) k) x ) -- ^ EM Ascent
stochasticIPCMEpoch validator nstps nbtch xzmp (k,eps) mlkl0 = do
    let xzss = M.toList xzmp
        (xs,zss) = unzip xzss
        mxs = sufficientStatistic <$> xs
        mhrms = [ expectationStep zs nhrmht
          | (zs,nhrmht) <- zip zss $ mlkl0 >$> mxs ]
        chn = chainCircuit mlkl0 $ proc mlkl -> do
            (mxs',mhrms') <- unzip ^<< minibatcher nbtch (zip mxs mhrms) -< ()
            let dfs = zipWith relativeEntropyDifferential mhrms' nhrmhts'
                (mlkld,nhrmhts') = propagate dfs mxs' mlkl
            let dmlkl = vanillaGradient mlkld
            mlkl' <- gradientCircuit (-eps) defaultAdamPursuit -< (mlkl, dmlkl)
            returnA -< Point . S.map (min 1e12) $ coordinates mlkl'
    mlkls <- realize $ streamChain nstps chn
    let mlkl' = last . takeWhile isBoundPoint $ take nstps mlkls
        vl = validator mlkl'
    putStrLn $ concat ["Epoch: ", show k, ", Log-Likelihood: " ++ show vl]
    return $!! ((mlkl',vl),mlkl')

fitCBCM
    :: ( KnownNat n, KnownNat k, ExponentialFamily x, Ord (SamplePoint x)
       , Propagate Natural Tensor (Neurons n) x, NFData a )
    => Double -- Fraction to pretrain as a poisson model
    -> [Double]
    -> Int
    -> SampleMap (Neurons n) x -- ^ Training data
    -> (Natural # IPCM n k x -> a)
    -> (Natural # CBCM n k x -> a)
    -> Natural # IPCM n k x
    -> (Natural # CBCM n k x, [a])
fitCBCM frc epss nstps tzxmp validator1 validator2 mlkl0 =
    let (epss1,epss2) = splitAt (round . (*frc) . fromIntegral $ length epss) epss
        (mlkl1,ascnt1) = fitIPCM epss1 nstps tzxmp validator1 mlkl0
        mlkl1' = joinCBCM (-1) mlkl1
        (mlkls,vls) = unzip $ L.scanl' scanner (mlkl1',validator2 mlkl1') epss2
     in (last mlkls, ascnt1 ++ tail vls)
    where
        scanner (mlkl,_) eps =
            let mlkl' = minimalCBCMEpoch nstps tzxmp mlkl eps
             in (mlkl',) $!! validator2 mlkl'


minimalCBCMEpoch
    :: ( KnownNat n, KnownNat k, Eq (SamplePoint x)
       , ExponentialFamily x , Transition Natural Mean (CBMixture n k) )
    => Int
    -> SampleMap (Neurons n) x -- ^ Training Data
    -> Natural # CBCM n k x
    -> Double
    -> Natural # CBCM n k x
minimalCBCMEpoch nstps xzmp mlkl0 eps =
    let xs = M.keys xzmp
        mxs = sufficientStatistic <$> xs
        zss = M.elems xzmp
        mhrms = [ expectationStep zs nhrmht | (zs,nhrmht) <- zip zss $ mlkl0 >$> mxs ]
        diff mlkl =
            let dfs = parMap rdeepseq (uncurry relativeEntropyDifferential)
                    $ zip mhrms nhrmhts
                (f',nhrmhts) = propagate dfs mxs mlkl
             in f'
        chn = chainCircuit mlkl0 $ proc mlkl -> do
                let dmlkl = vanillaGradient $ diff mlkl
                mlkl' <- gradientCircuit (-eps) defaultAdamPursuit -< (mlkl, dmlkl)
                returnA -< rectifyMinimalCBCM mlkl'
     in runIdentity $ iterateChain nstps chn

maximalCBCMEpoch
    :: ( KnownNat n, KnownNat k, KnownNat s )
    => Int
    -> SampleMap (Neurons n) (Categorical s) -- ^ Training Data
    -> Natural # CBMixture n k <* Categorical s
    -> Double
    -> Natural # CBMixture n k <* Categorical s
maximalCBCMEpoch nstps xzmp mlkl0 eps =
    let xs = M.keys xzmp
        mxs = sufficientStatistic <$> xs
        zss = M.elems xzmp
        mhrms = [ expectationStep zs nhrmht | (zs,nhrmht) <- zip zss $ mlkl0 >$> mxs ]
        diff mlkl =
            let dfs = parMap rdeepseq (uncurry relativeEntropyDifferential)
                    $ zip mhrms nhrmhts
                (f',nhrmhts) = propagate dfs mxs mlkl
             in f'
        chn = chainCircuit mlkl0 $ proc mlkl -> do
                let dmlkl = vanillaGradient $ diff mlkl
                mlkl' <- gradientCircuit (-eps) defaultAdamPursuit -< (mlkl, dmlkl)
                returnA -< rectifyMaximalCBCM mlkl'
     in runIdentity $ iterateChain nstps chn

splitMaximalCM
    :: (LegendreExponentialFamily z, KnownNat k)
    => Natural # z <* Categorical k
    -> S.Vector (k + 1) (Natural # z)
splitMaximalCM mlkl =
    fst . splitNaturalMixture $ join mlkl 0

joinMaximalCM
    :: (LegendreExponentialFamily z, KnownNat k)
    => S.Vector (k + 1) (Natural # z)
    -> Natural # z <* Categorical k
joinMaximalCM mxmdls =
    fst . split $ joinNaturalMixture mxmdls 0

fitMaximalCBCM
    :: (KnownNat k, KnownNat n, KnownNat s, NFData a)
    => Double
    -> [Double]
    -> Int
    -> SampleMap (Neurons n) (Categorical k) -- ^ Training data
    -> (Natural # Mixture (Neurons n) k <* Categorical s -> a)
    -> (Natural # CBMixture n k <* Categorical s -> a)
    -> Natural # Mixture (Neurons n) k <* Categorical s
    -> (Natural # CBMixture n k <* Categorical s, [a])
fitMaximalCBCM frc epss nstps tzxmp validator1 validator2 mlkl0 =
    let (epss1,epss2) = splitAt (round . (*frc) . fromIntegral $ length epss) epss
        (mlkl1,ascnt1) = fitIPCM epss1 nstps tzxmp validator1 mlkl0
        mlkl1' = joinMaximalCM . S.map (joinCBMixture (-1)) $ splitMaximalCM mlkl1
        --mlkl1' = joinCoMBasedMaximalCM (-1) mlkl1
        (mlkls,vls) = unzip $ L.scanl' scanner (mlkl1',validator2 mlkl1') epss2
     in (last mlkls, ascnt1 ++ tail vls)
    where
        scanner (mlkl,_) eps =
            let mlkl' = maximalCBCMEpoch nstps tzxmp mlkl eps
             in (mlkl',) $!! validator2 mlkl'

comFisherInformation
    :: (KnownNat m, KnownNat k)
    => Natural # CBCM k m VonMises
    -> Double
    -> Double
comFisherInformation mlkl x =
    let cvr = snd . splitMultivariateNormal . comMixtureCovariance $ mlkl >.>* x
        nzx = snd $ split mlkl
        dsx = Point $ S.fromTuple (-sin x, cos x)
        dmu = coordinates $ nzx >.> dsx
     in S.dotProduct dmu $ S.matrixVectorMultiply cvr dmu

poissonFisherInformation
    :: (KnownNat n, KnownNat k)
    => Natural # IPCM k n VonMises
    -> Double
    -> Double
poissonFisherInformation mlkl x =
    let cvr = snd . splitMultivariateNormal . poissonMixtureCovariance $ mlkl >.>* x
        nzx = snd $ split mlkl
        dsx = Point $ S.fromTuple (-sin x, cos x)
        dmu = coordinates $ nzx >.> dsx
     in S.dotProduct dmu $ S.matrixVectorMultiply cvr dmu

--- Auxilliary Functions ---

normalizePoissonWeights
    :: ( KnownNat n, KnownNat k )
    => Natural # IPCM n k VonMises
    -> Natural # IPCM n k VonMises
normalizePoissonWeights lkl =
    let xsmps = range 0 (2*pi) 100
        nks = coordinates . snd . splitNaturalMixture <$> lkl >$>* xsmps
        nk' = S.generate $ \fn -> maximum $ (`S.index` fn) <$> nks
        shft = average $ S.toList nk'
        (nnk,fnx) = split lkl
        (fnk,nk) = split nnk
        nnk' = join fnk . (+ realToFrac shft) $ nk - Point nk'
     in join nnk' fnx

normalizeCoMBasedWeights
    :: (KnownNat n, KnownNat k)
    => Natural # CBCM n k VonMises
    -> Natural # CBCM n k VonMises
normalizeCoMBasedWeights lkl =
    let xsmps = range 0 (2*pi) 100
        nks = coordinates . toNatural . snd . split . toMean <$> lkl >$>* xsmps
        nk' = S.generate $ \fn -> maximum $ (`S.index` fn) <$> nks
        shft = average $ S.toList nk'
        (shps,lkl1) = splitCBCM lkl
        (fnx,nns,nk) = splitIPCM lkl1
        lkl1' = joinIPCM fnx nns . (+ realToFrac shft) $ nk - Point nk'
     in joinCBCM shps lkl1'


