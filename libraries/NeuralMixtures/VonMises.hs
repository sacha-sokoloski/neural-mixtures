{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE TypeApplications,Arrows,DeriveGeneric #-}
module NeuralMixtures.VonMises
    (
    -- * Variables
      mnang
    , mxang
    -- * Construction
    , toCircularDatamap
    , randomVonMises
    -- * Sampling
    , noisySample
    -- * Training
    , fitVonMisesIndependent
    , sinusoidalModulations
    ) where


--- Imports ---


-- Goal --

import Goal.Core
import Goal.Geometry
import Goal.Probability

import qualified Goal.Core.Vector.Storable as S
import qualified Data.Map.Strict as M

import Core
import NeuralMixtures


--- Variables ---


mnang,mxang :: Double
mnang = 0
mxang = 2*pi


--- Generation ---

toCircularDatamap
     :: KnownNat n
     => M.Map String [Response n] -> M.Map Double [Response n]
toCircularDatamap = M.mapKeys read


randomVonMises
    :: KnownNat n
    => Source # Normal -- ^ Precision Distribution
    -> Natural # Neurons n -- ^ Gains
    -> Random (Natural # Neurons n <* VonMises)
randomVonMises rprc lgns = do
    ntcs <- somewhatRandomTuningCurves rprc
    return $ joinVonMisesIndependent lgns ntcs

somewhatRandomTuningCurves
    :: KnownNat n
    => Source # Normal -- ^ Tuning Curve Log-Precision Distribution
    -> Random (S.Vector n (Natural # VonMises))
somewhatRandomTuningCurves rprc = do
    prcs <- S.replicateM $ exp <$> samplePoint rprc
    return $ S.zipWith tcFun convolutionalMus prcs
        where tcFun mu prc = toNatural . Point @ Source $ S.doubleton mu prc

convolutionalMus :: KnownNat n => S.Vector n Double
convolutionalMus = S.init $ S.range mnang mxang

-- | Splits a von mises population code.
splitVonMisesIndependent
    :: KnownNat k
    => Natural # Replicated k Poisson <* VonMises
    -> (Natural # Neurons k, S.Vector k (Natural # VonMises))
splitVonMisesIndependent lkl =
    let (nz0,nzx) = split lkl
        nxs = toRows nzx
        nz = nz0 + Point (S.map potential nxs)
     in (nz,nxs)

joinVonMisesIndependent
    :: KnownNat n
    => Natural # Neurons n -- ^ Gains
    -> S.Vector n (Natural # VonMises) -- ^ Von Mises Curves
    -> Natural # Replicated n Poisson <* VonMises -- ^ Population Likelihood
joinVonMisesIndependent nz0 nps =
    let mtx = fromRows nps
        nz = nz0 - Point (S.map potential nps)
     in join nz mtx

fitVonMisesIndependent
    :: KnownNat n
    => Double -- ^ learning rate
    -> Int -- ^ number of iterations
    -> M.Map Double [Response n]  -- ^ training data
    -> Natural # Replicated n Poisson <* VonMises -- ^ Population Likelihood
fitVonMisesIndependent eps nepchs zxmp =
    let gns = transition $ averageSufficientStatistic . concat $ M.elems zxmp
        mus = preferredStimuliFromData zxmp
        tcs = S.map (\mu -> toNatural . Point @ Source $ S.fromTuple (mu,1)) mus
        lkl0 = joinVonMisesIndependent gns tcs
     in vanillaGradientSequence (mapConditionalLogLikelihoodDifferential zxmp)
            eps defaultAdamPursuit lkl0 !! nepchs

preferredStimuliFromData
    :: KnownNat n
    => M.Map Double [Response n]  -- ^ training data
    -> S.Vector n Double
preferredStimuliFromData zxmp =
    let zxs = mapToData zxmp
        (zs,xs) = unzip zxs
     in S.generate $ \fnt ->
         let zis = fromIntegral . (`S.index` fnt) <$> zs
          in weightedCircularAverage $ zip zis xs

vonMisesIndependentMeans
    :: KnownNat k
    => Natural # Replicated k Poisson <* VonMises
    -> S.Vector k Double
vonMisesIndependentMeans lkl =
    let tcs = snd $ splitVonMisesIndependent lkl
     in S.map (S.head . coordinates . toSource) tcs

sinusoidalModulation'
    :: KnownNat n
    => Double
    -> Double
    -> S.Vector n Double
    -> Natural # Neurons n -- ^ Mixture parameters
sinusoidalModulation' gn shft mus =
    let md mu = gn * sin (mu + shft)
     in Point $ S.map md mus

sinusoidalModulations
    :: forall k n . (KnownNat k, KnownNat n)
    => Double -- ^ Gain
    -> Double -- ^ Categorical Bias
    -> Natural # Replicated n Poisson <* VonMises
    -> Natural # IPCM n k VonMises
sinusoidalModulations gn kbs lkl =
    let mus = vonMisesIndependentMeans lkl
        shfts = S.generate ( \k' ->
            2*pi*fromIntegral k' / fromIntegral (natVal (Proxy @ k)) )
        vmds = S.map (\shft -> sinusoidalModulation' gn shft mus) shfts
     in joinIPCM lkl vmds $ realToFrac kbs


--- Tuning ---


noisySample
    :: ( Map Natural f x2 VonMises, Generative Natural x2
       , SamplePoint x2 ~ (a, b) )
      => [Double]
      -> Int
      -> Double
      -> (Natural # f x2 VonMises)
      -> Random [(a, Double)]
noisySample stms n 0 mlkl = do
    zxss <- forM stms $ \stm -> do
        ns <- sample n $ mlkl >.>* stm
        return . zip (fst <$> ns) $ repeat stm
    shuffleList $ concat zxss

noisySample stms n prcs mlkl = do
    zxss <- forM stms $ \stm -> do
        let cm :: Source # VonMises
            cm = fromTuple (stm,prcs)
        xs <- sample n cm
        ns <- mapM (fmap fst . samplePoint) $ mlkl >$>* xs
        return . zip ns $ repeat stm
    shuffleList $ concat zxss


--- Internal ---


mapToData :: M.Map x [y] -> [(y,x)]
mapToData mp =
    let (xs,zss) = unzip $ M.toAscList mp
     in concat $ zipWith (\x zs -> zip zs $ repeat x) xs zss


