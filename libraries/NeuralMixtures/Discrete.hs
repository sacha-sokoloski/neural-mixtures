{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE TypeApplications,Arrows,DeriveGeneric,TupleSections #-}
module NeuralMixtures.Discrete
    (
    -- * Initialization
      toDiscreteDatamap
    , dataInitializeMinimalCM
    , dataInitializeMaximalCM
    ) where

--- Imports ---


-- Goal --

import Goal.Core
import Goal.Geometry
import Goal.Probability
import Goal.Graphical

import qualified Goal.Core.Vector.Storable as S
import qualified Data.Map.Strict as M
import qualified Data.Tuple as T

import Core
import NeuralMixtures


--- Functions ---


toDiscreteDatamap
     :: KnownNat n
     => M.Map String [Response n] -> (M.Map Int [Response n], M.Map Int String)
toDiscreteDatamap zxmp =
    let ctxs = zip [0..] $ M.keys zxmp
        xctmp = M.fromList ctxs
        ctxmp = M.fromList $ T.swap <$> ctxs
        zctmp = M.mapKeys (ctxmp M.!) zxmp
     in (zctmp, xctmp)

dataInitializeMinimalCM
    :: forall k n m . (KnownNat k, KnownNat n, KnownNat m)
    => (Double,Double) -- ^ Gain Noise
    -> M.Map Int [Response n]
    -> Random (Natural # IPCM n k (Categorical m))
dataInitializeMinimalCM mnmx zxmp = do
    let zxs = mapToData zxmp
    let fhrm :: Natural # Mixture (Neurons n) m
        fhrm = toNatural $ averageSufficientStatistic zxs
        (nz,nzm,_) = splitHarmonium fhrm
    gnss <- S.replicateM $ uniformInitialize mnmx
    return $ joinIPCM (join nz nzm) gnss 0

dataInitializeMaximalCM
    :: forall k n m . (KnownNat k, KnownNat n, KnownNat m)
    => (Double,Double) -- ^ Gain Noise
    -> M.Map Int [Response n]
    -> Random (Natural # Mixture (Neurons n) k <* Categorical m)
dataInitializeMaximalCM mnmx zxmp = do
    let zxs = mapToData zxmp
    let fhrm :: Natural # Mixture (Neurons n) m
        fhrm = toNatural $ averageSufficientStatistic zxs
        (nz,nzm,_) = splitHarmonium fhrm
    gnss0 <- S.replicateM $ uniformInitialize mnmx
    let nznm = fromRows $ toRows nzm S.++ S.replicate 0
        gnss = S.map (nz +) gnss0
    return $ join (joinNaturalMixture gnss 0) nznm


--- Internal ---


mapToData :: M.Map x [y] -> [(y,x)]
mapToData mp =
    let (xs,zss) = unzip $ M.toAscList mp
     in concat $ zipWith (\x zs -> zip zs $ repeat x) xs zss


