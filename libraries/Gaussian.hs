{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}
{-# LANGUAGE EmptyDataDecls,UndecidableInstances,Arrows,BangPatterns,TypeApplications #-}

module Gaussian
    ( -- * Covariances
      interpolateCovarianceFunction
    , multivariateNormalFanoFactors
      -- * Factor Analysis
    , FactorAnalysis
    , joinFactorAnalysis
    , splitFactorAnalysis
    , toMultivariateNormal
    , factorAnalysisExpectationMaximization
    , initializeFactorAnalysis
    , fitFactorAnalysis
    , initializeConditionalFactorAnalysis
    , fitConditionalFactorAnalysis
    , writeFactorAnalysis
    , factorAnalysisValidator
    , listCorrelations
    , filterFun
    -- * PCA
    , principalComponentAnalysis
    -- * Fisher Information
    , factorAnalysisLinearFisherInformations
    ) where

--- Imports ---


-- Package --

import Goal.Core
import Goal.Geometry
import Goal.Probability

import qualified Goal.Core.Vector.Storable as S
import qualified Goal.Core.Vector.Boxed as B
import qualified Goal.Core.Vector.Generic as G

import Core

import qualified Data.List as L
import qualified Data.Map.Strict as M

import Data.Complex


-- Statistics --


multivariateNormalFanoFactors
    :: KnownNat n => (Source # MultivariateNormal n) -> [Double]
multivariateNormalFanoFactors mnrm =
    let (mus,cvr) = splitMultivariateNormal mnrm
        vrs = S.takeDiagonal cvr
     in S.toList $ S.zipWith (/) vrs mus

filterFun :: RealFloat a => (a, b) -> Bool
filterFun (dta,_) = not $ isNaN dta

listCorrelations :: KnownNat n => S.Matrix n n Double -> [Double]
listCorrelations crrs = concat . zipWith drop [1..] $ S.toList <$> S.toList (S.toRows crrs)

principalComponentAnalysis :: KnownNat k => Source # MultivariateNormal k -> [(Double,Double)]
principalComponentAnalysis mnrm =
    let cvr = snd $ splitMultivariateNormal mnrm
        eigs = reverse . L.sort . map realPart . S.toList . fst $ S.eigens cvr
        expvr = scanl1 (+) eigs
     in zip eigs $ (/last expvr) <$> expvr

interpolateCovarianceFunction
    :: (KnownNat k, Ord x)
    => M.Map x (Source # MultivariateNormal k)
    -> x
    -> Source # MultivariateNormal k
interpolateCovarianceFunction mpnrms x =
    let kys = M.keys mpnrms
        ky = last $ last kys : takeWhile (<= x) kys
     in mpnrms M.! ky


--- Factor Analysis ---


data FactorAnalysis (n :: Nat) (k :: Nat)

joinFactorAnalysis
    :: (KnownNat n, KnownNat k)
    => S.Vector n Double -- ^ Mean bias
    -> S.Vector n Double -- ^ Variances
    -> S.Matrix n k Double -- ^ Interaction Parameters
    -> Source # FactorAnalysis n k
joinFactorAnalysis mus vrs mtx =
    Point $ mus S.++ vrs S.++ G.toVector mtx

splitFactorAnalysis
    :: (KnownNat n, KnownNat k)
    => Source # FactorAnalysis n k
    -> (S.Vector n Double, S.Vector n Double, S.Matrix n k Double)
splitFactorAnalysis (Point cs) =
    let (mus,cs') = S.splitAt cs
        (vrs,mtx) = S.splitAt cs'
     in (mus,vrs,G.Matrix mtx)

toMultivariateNormal
    :: (KnownNat n, KnownNat k)
    => Source # FactorAnalysis n k
    -> Source # MultivariateNormal n
toMultivariateNormal fan =
    let (mus,vrs,mtx) = splitFactorAnalysis fan
        mtx1 = S.matrixMatrixMultiply mtx (S.transpose mtx)
        mtx2 = S.diagonalMatrix vrs
     in joinMultivariateNormal mus $ mtx1 + mtx2

factorAnalysisExpectationMaximization
    :: forall n k . (KnownNat n, KnownNat k)
    => [S.Vector n Double]
    -> Source # FactorAnalysis n k
    -> Source # FactorAnalysis n k
factorAnalysisExpectationMaximization xs fan =
    let (_,vrs,wmtx) = splitFactorAnalysis fan
        wmtxtr = S.transpose wmtx
        vrinv = S.pseudoInverse $ S.diagonalMatrix vrs
        mlts = S.matrixMatrixMultiply (S.matrixMatrixMultiply wmtxtr vrinv) wmtx
        gmtx = S.pseudoInverse $ S.matrixIdentity + mlts
        xht = average xs
        nxht = S.scale (-1) xht
        rsds = [ S.add x nxht | x <- xs ]
        mlts' = S.matrixMatrixMultiply (S.matrixMatrixMultiply gmtx wmtxtr) vrinv
        muhts = S.matrixVectorMultiply mlts' <$> rsds
        invsgm = S.pseudoInverse . (gmtx +) . S.averageOuterProduct $ zip muhts muhts
        wmtx0 = S.averageOuterProduct $ zip rsds muhts
        wmtx' = S.matrixMatrixMultiply wmtx0 invsgm
        vrs0 = S.withMatrix (S.scale (-1)) . S.matrixMatrixMultiply wmtx $ S.transpose wmtx0
        smtx = S.averageOuterProduct $ zip rsds rsds
        vrs' = S.takeDiagonal $ smtx + vrs0
     in joinFactorAnalysis xht vrs' wmtx'

initializeFactorAnalysis
    :: forall n k . (KnownNat n, KnownNat k)
    => [Response n] -> Random (Source # FactorAnalysis n k)
initializeFactorAnalysis zs0 = do
    let zs = S.map fromIntegral <$> zs0
        mus = average zs
        mu2s = average $ square <$> zs
        vrs = mu2s - square mus
        fa0 = joinFactorAnalysis mus vrs 0
        mubnds :: B.Vector n (Double,Double)
        mubnds = B.zip (B.replicate (-0.01)) (B.replicate 0.01)
        vrbnds :: B.Vector n (Double,Double)
        vrbnds = B.zip (B.replicate 0.9) (B.replicate 1.1)
        mtxbnds :: B.Vector (n*k) (Double,Double)
        mtxbnds = B.zip (B.replicate (-0.1)) (B.replicate 0.1)
    fa1 <- uniformInitialize' $ mubnds B.++ vrbnds B.++ mtxbnds
    return $ fa0 + fa1

fitFactorAnalysis
    :: (KnownNat n, KnownNat k)
    => [Response n]
    -> Source # FactorAnalysis n k
    -> Source # FactorAnalysis n k
fitFactorAnalysis zs =
    factorAnalysisExpectationMaximization (S.map fromIntegral <$> zs)

writeFactorAnalysis
    :: forall k n s . (KnownNat k, KnownNat n, KnownNat s)
    => String -- ^ Load Path
    -> B.Vector s (Source # FactorAnalysis n k) -- ^ Mixture Likelihood
    -> IO ()
writeFactorAnalysis ldpth lkl = do
    createDirectoryIfMissing True ldpth
    let n = natValInt (Proxy @ n)
        k = natValInt (Proxy @ k)
        flpth = parametersPath ldpth
    writeFile flpth $ show (n,k, concat . B.toList $ listCoordinates <$> lkl)

factorAnalysisValidator
    :: forall m k . (KnownNat m, KnownNat k)
    => [Response k]
    -> Source # FactorAnalysis k m
    -> Double
factorAnalysisValidator zs fa =
    let xs :: [S.Vector k Double]
        xs = S.map fromIntegral <$> zs
     in average $ multivariateNormalLogLikelihood (toMultivariateNormal fa) <$> xs

initializeConditionalFactorAnalysis
    :: (KnownNat n, KnownNat k, KnownNat s)
    => B.Vector s [Response n]
    -> Random (B.Vector s (Source # FactorAnalysis n k))
initializeConditionalFactorAnalysis = mapM initializeFactorAnalysis

multivariateNormalLogLikelihood :: KnownNat n => Source # MultivariateNormal n -> S.Vector n Double -> Double
multivariateNormalLogLikelihood p xs =
    let (mus,sgma) = splitMultivariateNormal p
        nrm = (* (-0.5)) . log . S.determinant $ scaleMatrix (2*pi) sgma
        dff = S.add xs (S.scale (-1) mus)
        expval = S.dotProduct dff $ S.matrixVectorMultiply (S.pseudoInverse sgma) dff
     in nrm - expval / 2

scaleMatrix :: Double -> S.Matrix m n Double -> S.Matrix m n Double
scaleMatrix x = S.withMatrix (S.scale x)


fitConditionalFactorAnalysis
    :: ( KnownNat k, KnownNat n, KnownNat s )
    => Int
    -> SampleMap (Neurons n) (Categorical s) -- ^ Training Data
    -> (B.Vector s (Source # FactorAnalysis n k) -> a)
    -> B.Vector s (Source # FactorAnalysis n k)
    -> (B.Vector s (Source # FactorAnalysis n k), [a])
fitConditionalFactorAnalysis nepchs tzxmp validator fas0 =
    let tzxs = B.generate (\n -> tzxmp M.! fromIntegral n)
        fas = take nepchs $ iterate (B.zipWith fitFactorAnalysis tzxs) fas0
     in (last fas, validator <$> fas)


--- Fisher Information ---


factorAnalysisLinearFisherInformation0
    :: ( KnownNat k, KnownNat n)
    => (Double,Double) -- Discrimation Points
    -> (Source # FactorAnalysis k n, Source # FactorAnalysis k n)
    -> (Double,Double) -- Location/FI
factorAnalysisLinearFisherInformation0 (lwx,upx) (lwfa,upfa) =
    let [lwnrm,upnrm] = toMultivariateNormal <$> [lwfa,upfa]
        (_,mux,fsh) = linearFisherize (lwx,upx) (lwnrm,upnrm)
     in (mux,fsh)

factorAnalysisLinearFisherInformations
    :: ( KnownNat k, KnownNat n)
    => M.Map Double (Source # FactorAnalysis k n)
    -> [(Double,Double)] -- location/Naive/Corrected
factorAnalysisLinearFisherInformations xfamp =
    let (xs,fas) = unzip $ M.assocs xfamp
        lwupxs = zip xs $ tail xs
        lwupfas = zip fas $ tail fas
     in zipWith factorAnalysisLinearFisherInformation0 lwupxs lwupfas



--- Instances ---


instance (KnownNat n, KnownNat k) => Manifold (FactorAnalysis n k) where
    type Dimension (FactorAnalysis n k) = 2*n + k*n

instance (KnownNat n, KnownNat k) => Statistical (FactorAnalysis n k) where
    type SamplePoint (FactorAnalysis n k) = (S.Vector n Double, S.Vector k Double)


