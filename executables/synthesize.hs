{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}

{-# LANGUAGE
    FlexibleContexts,
    GADTs,
    ScopedTypeVariables,
    DataKinds,
    TypeApplications,
    BangPatterns,
    TypeOperators
    #-}


--- Imports ---


-- Goal --

import Goal.Core
import Goal.Geometry
import Goal.Probability

import Core
import NeuralMixtures
import NeuralMixtures.VonMises

import qualified Goal.Core.Vector.Storable as S


--- Globals ---


vdst :: String
vdst = "poisson"

cdst :: String
cdst = "com-based"


--- IO ---


-- Main --

main :: IO ()
main = do

    let hdrstr = "Generate a randomized conditional mixture model and and a corresponding synthetic dataset."
        prgstr =
            "Generate a randomized conditional mixture model, and a synthetic dataset from the model.  Running this executable actually generates two models and two datasets within the specified directory, one with Poisson neurons and the other with CoM-based neurons. Model and data generation parameters can be specified with command line arguments."
        opts = info (syntheticOpts <**> helper) (fullDesc <> progDesc prgstr <> header hdrstr)

    runOpts =<< execParser opts

-- CLI --

data SyntheticOpts =
    SyntheticOpts
        String NatNumber NatNumber Int Int Double Double Double Double Double Double Double

syntheticOpts :: Parser SyntheticOpts
syntheticOpts = SyntheticOpts
    <$> option str
        ( short 'e'
        <> long "experiment-name"
        <> help "Name of this synthetic experiment."
        <> showDefault
        <> value "synthetic" )
    <*> option auto
        ( short 'n'
        <> long "n-neurons"
        <> help "Number of neurons in the model population."
        <> showDefault
        <> value 20 )
    <*> option auto
        ( short 'k'
        <> long "k-components"
        <> help "Number of mixture components-1 in the model population."
        <> showDefault
        <> value 4 )
    <*> option auto
        ( short 's'
        <> long "n-oris"
        <> help "Number of unique orientations to generate synthetic data from, spread evenly over the circle."
        <> showDefault
        <> value 8 )
    <*> option auto
        ( short 'S'
        <> long "n-samples"
        <> help "Number of samples to generate from the model at each orientation."
        <> showDefault
        <> value 200 )
    <*> option auto
        ( short 'N'
        <> long "sensory-noise"
        <> help "Precision of sensory noise (larger means less noise, 0 turns noise off)."
        <> showDefault
        <> value 0 )
    <*> option auto
        ( short 'g'
        <> long "gain-mu"
        <> help "The average logarithm of the gains of the tuning curves."
        <> showDefault
        <> value 0 )
    <*> option auto
        ( short 'G'
        <> long "gain-vr"
        <> help "The variance of the log-gains."
        <> showDefault
        <> value 0.1 )
    <*> option auto
        ( short 'p'
        <> long "precision-mu"
        <> help "The average logarithm of the precisions of the tuning curves."
        <> showDefault
        <> value 0 )
    <*> option auto
        ( short 'P'
        <> long "log-precision-vr"
        <> help "The variance of the log-precisions."
        <> showDefault
        <> value 0.1 )
    <*> option auto
        ( short 'h'
        <> long "min-shape"
        <> help "The lower-bound of the shape parameter for CoM-based models."
        <> showDefault
        <> value (-1.2) )
    <*> option auto
        ( short 'H'
        <> long "max-shape"
        <> help "The upper-bound of the shape parameter for CoM-based models."
        <> showDefault
        <> value (-0.8) )

runOpts :: SyntheticOpts -> IO ()
runOpts (SyntheticOpts expmnt n k nstms nsmps sns lgmu lgsd lpmu lpsd mnshp mxshp) = do

    let stms = init $ range mnang mxang (nstms + 1)

    case (someNatVal n, someNatVal k) of

      (SomeNat (Proxy :: Proxy n), SomeNat (Proxy :: Proxy k)) -> do

        let lgnnrm,lprcnrm :: Source # Normal
            lgnnrm = fromTuple (lgmu,lgsd)
            lprcnrm = fromTuple (lpmu,lpsd)

        lgns <- realize $ initialize lgnnrm
        vlkl0 <- realize $ randomVonMises lprcnrm lgns

        lgnss <- realize $ S.replicateM $ initialize lgnnrm

        let vlkl :: Natural # IPCM n k VonMises
            vlkl = normalizePoissonWeights $ joinIPCM vlkl0 lgnss 0

        vzxs <- realize $ noisySample stms nsmps sns vlkl

        let vldpth = analysisPath expmnt vdst ["true"]

        writeCM vldpth vlkl
        writeNeuralData expmnt vdst vzxs

        shps <- realize $ uniformInitialize (mnshp,mxshp)

        let clkl = joinCBCM shps vlkl

        czxs <- realize $ noisySample stms nsmps sns clkl

        let cldpth = analysisPath expmnt cdst ["true"]

        writeNeuralData expmnt cdst czxs
        writeCM cldpth clkl
