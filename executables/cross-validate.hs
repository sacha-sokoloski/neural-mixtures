#! /usr/bin/env stack
-- stack runghc --

{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}

{-# LANGUAGE
    FlexibleContexts,
    TypeFamilies,
    TypeOperators,
    TypeApplications,
    ScopedTypeVariables,
    TupleSections,
    DataKinds
    #-}


--- Imports ---


import Core
import NeuralMixtures
import NeuralMixtures.VonMises
import NeuralMixtures.Discrete
import Gaussian

import Goal.Core
import Goal.Geometry
import Goal.Probability
import Goal.Graphical hiding (FactorAnalysis)

import qualified Goal.Core.Vector.Storable as S
import qualified Goal.Core.Vector.Boxed as B
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import qualified Statistics.Sample as STAT

--import qualified Data.Map as M
import qualified Data.List as L

import Control.Concurrent.Async


--- Globals ---

comrto :: Double
comrto = 0.8

llnm,pstnm,ffnm,crrnm,skwnm,krtnm :: String
llnm = "ll"
ffnm = "ff"
pstnm = "pst"
crrnm = "crr"
skwnm = "skw"
krtnm = "krt"

labelFun :: String -> String
labelFun nm
  | nm == llnm = "Log-Likelihood"
  | nm == pstnm = "Log-Posterior"
  | nm == ffnm = "Fano Factor r^2"
  | nm == crrnm = "Noise Correlation r^2"
  | nm == skwnm = "Diag. Skewness r^2"
  | nm == krtnm = "Diag. Kurtosis r^2"
  | otherwise = "Bad validation name"



--- Program ---


-- Main --

main :: IO ()
main = do

    let prgstr = "Cross-validate a model against a variety of metrics."
        hdrstr = "Cross-validate the performance of a variety of CMs, as well as factor analysis, and plot the results."
        opts = info (allOpts <**> helper) (fullDesc <> progDesc prgstr <> header hdrstr)
    runOpts =<< execParser opts

-- CLI --

data ValidationOpts = ValidationOpts [String] [Int] Int Int Double Double Int

validationOpts :: Parser ValidationOpts
validationOpts = ValidationOpts
    <$> many (strArgument
        ( metavar "Model subdirectories (i.e. tuning/com-ness") )
    <*> option auto
        ( short 'k'
        <> long "components"
        <> help "Number of mixture components - 1. Note that this argument must be wrapped in quotes."
        <> showDefault
        <> value [0,2,4,9,14,19,24,29,34,39,44,49] )
    <*> option auto
        ( short 'f'
        <> long "f-folds"
        <> help "Number of folds per cross-validation."
        <> showDefault
        <> value 10 )
    <*> option auto
        ( short 'e'
        <> long "n-epochs"
        <> help "Number of training epochs per simulation."
        <> showDefault
        <> value 500 )
    <*> option auto
        ( short 'l'
        <> long "max-learning-rate"
        <> help "The maximum learning rate."
        <> showDefault
        <> value 3e-3 )
    <*> option auto
        ( short 'L'
        <> long "min-learning-rate"
        <> help "The minimum learning rate."
        <> showDefault
        <> value 1e-3 )
    <*> option auto
        ( short 's'
        <> long "n-steps"
        <> help "Number of steps (for gradient-ascent on maximization step)."
        <> showDefault
        <> value 50 )


data AllOpts = AllOpts ExperimentOpts ValidationOpts

allOpts :: Parser AllOpts
allOpts = AllOpts <$> experimentOpts <*> validationOpts

runOpts :: AllOpts -> IO ()
runOpts ( AllOpts (ExperimentOpts expmnt dst)
    (ValidationOpts sbdrs ks fld nepchs mxeps mneps nstps) ) = do

    (n,s,zxs0) <- readNeuralData expmnt dst

    let ldpth = analysisPath expmnt dst sbdrs

    putStrLn "\nNumber of Neurons:"
    print n

    putStrLn "\nNumber of Latents - 1:"
    print ks

    putStrLn "\nNumber of Stimuli:"
    print s

    case (someNatVal $ fromIntegral n, someNatVal (s-1)) of

      (SomeNat prxn, SomeNat prxs) -> do

        let zxs = strengthenNeuralData zxs0

        let zxmp = conditionalDataMap zxs
        let tvmps = kFoldMap fld zxmp

        putStrLn "\nNumber of Samples:"
        print . M.elems $ length <$> zxmp

        putStrLn "\nSimulation:"

        cvlss <- forConcurrently ks $ \k ->

            case someNatVal $ fromIntegral k of

              SomeNat prxk -> do

                let validator (tzxmp,vzxmp) = realize $ case sbdrs of

                      ["von-mises",frm] ->
                          validateVonMises frm prxn prxk nepchs mxeps mneps nstps tzxmp vzxmp

                      ["discrete",frm] ->
                          validateDiscrete frm prxn prxk prxs nepchs mxeps mneps nstps tzxmp vzxmp

                      ["maximal",frm] ->
                          validateMaximal frm prxn prxk prxs nepchs mxeps mneps nstps tzxmp vzxmp

                      ["factor-analysis"] ->

                          validateFactorAnalysis prxn prxk prxs nepchs tzxmp vzxmp

                      _ -> error "Unrecognized directory structure"

                prmevlss <- mapConcurrently validator tvmps

                processValidationIteration k prmevlss

        let (nprms,statss) = unzip cvlss

        sequence_ $ do

            (nms,mus,sds,avgascnts) <- L.unzip4 <$> L.transpose statss
            let nm = head nms

            let cvs = L.zip4 ks nprms mus sds
                ascnts = L.transpose $ zipWith (:) (fromIntegral <$> ks) avgascnts

            return $ do

                goalExport ldpth (nm ++ "-cross-validation-results") cvs
                goalExport ldpth (nm ++ "-cross-validation-history") ascnts
                runGnuplotWithVariables ldpth
                    "plot-files/cross-validate/cross-validation-history"
                    [("validation",nm), ("lbl",labelFun nm)]
                runGnuplotWithVariables ldpth
                    "plot-files/cross-validate/cross-validation-results"
                    [("validation",nm), ("lbl",labelFun nm)]


-- Functions --


processValidationIteration
    :: Int
    -> [(Int, [(String,[Double])])]
    -> IO (Int, [(String,Double,Double,[Double])])
processValidationIteration k prmevlss = do

    let (nprms,evlss) = unzip prmevlss
        nprm = head nprms

    prntstats <- sequence $ do
        nmevls <- L.transpose evlss
        let (nms,evls) = unzip nmevls
            nm = head nms
            lsts = last <$> evls
            (mu,cvr) = estimateMeanVariance lsts
            avgascnt = average <$> L.transpose evls
            prnts = concat ["\n",nm, ": ",showFFloat (Just 4) mu ""]
        return $ do
            return $!! (prnts,(nm,mu,sqrt cvr,avgascnt))

    let (prnts,stats) = unzip prntstats

    putStrLn $!! concat ([ "# Latents: " ++ show k, ", " , "# Parameters: " ++ show nprm ] ++ prnts)

    return (nprm,stats)


--- Validation Functions ---


validateMaximal
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => String
    -> Proxy n
    -> Proxy k
    -> Proxy s
    -> Int
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> Random (Int, [(String,[Double])])
validateMaximal mdl _ _ _ nepchs mxeps mneps nstps tzxmp0 vzxmp0 = do

    let tzxmp = fst . toDiscreteDatamap $ tzxmp0
        vzxmp = fst . toDiscreteDatamap $ vzxmp0
        epss = epsilonRange mxeps mneps nepchs

    let validator1 lkl = ( discreteBayesianValidator vzxmp lkl
                         , poissonMomentValidator vzxmp lkl )
    let validator2 lkl = ( discreteBayesianValidator vzxmp lkl
                         , comMomentValidator vzxmp lkl )

    mlkl0 :: Natural # Mixture (Neurons n) k <* Categorical s
        <- dataInitializeMaximalCM (-1e-4,1e-4) tzxmp

    case mdl of

      "poisson" -> do

          let (mlkl1,vls) = fitIPCM epss nstps tzxmp validator1 mlkl0
              (llpsts,mmnts) = unzip vls
              (lls,psts) = unzip llpsts
              (ffs,crrs,skws,krts) = L.unzip4 mmnts
          return $!! ( S.length $ coordinates mlkl1
                 , [(llnm,lls),(pstnm,psts),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

      "com-based" -> do

          let (mlkl1,vls) = fitMaximalCBCM
                  comrto epss nstps tzxmp validator1 validator2 mlkl0
              (llpsts,mmnts) = unzip vls
              (lls,psts) = unzip llpsts
              (ffs,crrs,skws,krts) = L.unzip4 mmnts
          return $!! ( S.length $ coordinates mlkl1
                 , [(llnm,lls),(pstnm,psts),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

      _ -> error "Form not supported"

validateDiscrete
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => String
    -> Proxy n
    -> Proxy k
    -> Proxy s
    -> Int
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> Random (Int, [(String,[Double])])
validateDiscrete mdl _ _ _ nepchs mxeps mneps nstps tzxmp0 vzxmp0 = do

    let tzxmp = fst . toDiscreteDatamap $ tzxmp0
        vzxmp = fst . toDiscreteDatamap $ vzxmp0
        epss = epsilonRange mxeps mneps nepchs

    let validator1 lkl = ( discreteBayesianValidator vzxmp lkl
                         , poissonMomentValidator vzxmp lkl )
    let validator2 lkl = ( discreteBayesianValidator vzxmp lkl
                         , comMomentValidator vzxmp lkl )

    mlkl0 :: Natural # IPCM n k (Categorical s)
        <- dataInitializeMinimalCM (-1e-4,1e-4) tzxmp

    case mdl of

      "poisson" -> do

          let (mlkl1,vls) = fitIPCM epss nstps tzxmp validator1 mlkl0
              (llpsts,mmnts) = unzip vls
              (lls,psts) = unzip llpsts
              (ffs,crrs,skws,krts) = L.unzip4 mmnts
          return $!! ( S.length $ coordinates mlkl1
                 , [(llnm,lls),(pstnm,psts),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

      "com-based" -> do

          let (mlkl1,vls) = fitCBCM
                  comrto epss nstps tzxmp validator1 validator2 mlkl0
              (llpsts,mmnts) = unzip vls
              (lls,psts) = unzip llpsts
              (ffs,crrs,skws,krts) = L.unzip4 mmnts
          return $!! ( S.length $ coordinates mlkl1
                 , [(llnm,lls),(pstnm,psts),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

      _ -> error "Form not supported"

validateVonMises
    :: forall n k . (KnownNat n, KnownNat k)
    => String
    -> Proxy n
    -> Proxy k
    -> Int
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> Random (Int, [(String,[Double])])
validateVonMises frm _ _ nepchs mxeps mneps nstps tzxmp0 vzxmp0 = do

    let epss = epsilonRange mxeps mneps nepchs
        tzxmp = toCircularDatamap tzxmp0
        vzxmp = toCircularDatamap vzxmp0

    let lkl0 = fitVonMisesIndependent 0.05 500 vzxmp

    let mlkl0 :: Natural # IPCM n k VonMises
        mlkl0 = sinusoidalModulations 0.2 0 lkl0

    let validator1 lkl = (mapConditionalLogLikelihood vzxmp lkl, poissonMomentValidator vzxmp lkl)
    let validator2 lkl = (mapConditionalLogLikelihood vzxmp lkl, comMomentValidator vzxmp lkl)

    case frm of

        "poisson" -> do

            let (mlkl1,vls) = fitIPCM epss nstps tzxmp validator1 mlkl0
                (lls,mmnts) = unzip vls
                (ffs,crrs,skws,krts) = L.unzip4 mmnts
            return $!! ( S.length $ coordinates mlkl1
                   , [(llnm,lls),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

        "com-based" -> do

            let (mlkl1,vls) = fitCBCM
                    comrto epss nstps tzxmp validator1 validator2 mlkl0
                (lls,mmnts) = unzip vls
                (ffs,crrs,skws,krts) = L.unzip4 mmnts
            return $!! ( S.length $ coordinates mlkl1
                   , [(llnm,lls),(ffnm,ffs),(crrnm,crrs),(skwnm,skws),(krtnm,krts)] )

        _ -> error "Form not supported"


validateFactorAnalysis
    :: forall n k s . (KnownNat n, KnownNat s, KnownNat k)
    => Proxy n
    -> Proxy k
    -> Proxy s
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> Random (Int, [(String,[Double])])
validateFactorAnalysis _ _ _ nepchs tzxmp0 vzxmp0 = do

    let tzxmp = fst $ toDiscreteDatamap tzxmp0
        vzxmp = fst $ toDiscreteDatamap vzxmp0

    let xss :: B.Vector (s+1) [Response n]
        xss = fromJust . B.fromList $ M.elems tzxmp

    fas0 :: B.Vector (s+1) (Source # FactorAnalysis n k)
        <- initializeConditionalFactorAnalysis xss

    let nprms = B.sum $ length . listCoordinates <$> fas0

    let validator = factorAnalysisBayesianValidator vzxmp
        (_,vls) = fitConditionalFactorAnalysis nepchs tzxmp validator fas0

        (lls,psts,ffs,crrs) = L.unzip4 vls

    return $!! (nprms,[(llnm,lls),(pstnm,psts),("ffs",ffs),("crrs",crrs)])


--- Validators ---


factorAnalysisBayesianValidator
    :: forall n k s . ( KnownNat n, KnownNat k, KnownNat s )
    => M.Map Int [Response n] -- ^ Validation Samples
    -> B.Vector (s+1) (Source # FactorAnalysis n k)
    -> (Double,Double,Double,Double)
factorAnalysisBayesianValidator vzxmp mlkl =
    let prrs = M.elems $ fromIntegral . length <$> vzxmp
        (truxs,cts) = unzip $ factorAnalysisPosteriors prrs vzxmp mlkl
        pst = posteriorPointComparison $ filter (\(trux,ct) ->  density ct trux > 0)  $ zip truxs cts
        ll = average $ zipWith factorAnalysisValidator (M.elems vzxmp) $ B.toList mlkl
        (ffs,crrs) = unzip $ do
            (zs,fa) <- zip (M.elems vzxmp) $ B.toList mlkl
            let dtanrm,fanrm :: Source # MultivariateNormal n
                dtanrm = mle $ S.map realToFrac <$> zs
                fanrm = toMultivariateNormal fa
                dtacrrs =  listCorrelations $ multivariateNormalCorrelations dtanrm
                facrrs = listCorrelations $ multivariateNormalCorrelations fanrm
                dtaffs = multivariateNormalFanoFactors dtanrm
                faffs = multivariateNormalFanoFactors fanrm
            return ( filter filterFun $ zip dtaffs faffs
                   , filter filterFun $ zip dtacrrs facrrs )

     in (ll,pst, rSquared' $ concat ffs, rSquared' $ concat crrs)

factorAnalysisPosteriors
    :: ( KnownNat n, KnownNat k, KnownNat s )
    => [Double]
    -> M.Map Int [Response n] -- ^ Validation Samples
    -> B.Vector (s+1) (Source # FactorAnalysis n k)
    -> [(Int, Mean # Categorical s)]
factorAnalysisPosteriors prrs vzixmp mlkl = do
    let nzks = B.toList mlkl
    (trux,zs0) <- M.toList vzixmp
    let zs = S.map fromIntegral <$> zs0
    dns0s <- L.transpose $ (`densities` zs) . toMultivariateNormal <$> nzks
    let mx = toCategorical prrs dns0s
    return (trux,mx)

minimalDiscretePosteriors
    :: forall f n g s .
        ( KnownNat n, KnownNat s, Map Natural f g (Categorical s)
        , ObservablyContinuous Natural g, Observation g ~ Response n )
    => [Double]
    -> M.Map Int [Response n] -- ^ Validation Samples
    -> Natural # f g (Categorical s)
    -> [(Int, Mean # Categorical s, Response n)]
minimalDiscretePosteriors prrs vzixmp mlkl = do
    let xs = sampleSpace (Proxy @ (Categorical s))
        nzks = mlkl >$>* xs
    (trux,zs) <- M.toList vzixmp
    (z,dns0s) <- zip zs . L.transpose $ (`observableDensities` zs) <$> nzks
    let mx = toCategorical prrs dns0s
    return (trux,mx,z)

discreteBayesianValidator
    :: forall f n g s .
        ( KnownNat n, KnownNat s, Map Natural f g (Categorical s)
        , ObservablyContinuous Natural g, Observation g ~ Response n
        , LogLikelihood Natural g (Response n) )
    => M.Map Int [Response n] -- ^ Validation Samples
    -> Natural # f g (Categorical s)
    -> (Double,Double)
discreteBayesianValidator vzxmp mlkl =
    let prrs = M.elems $ fromIntegral . length <$> vzxmp
        (truxs,cts,_) = unzip3 $ minimalDiscretePosteriors prrs vzxmp mlkl
        pst = posteriorPointComparison $ zip truxs cts
        ll = mapConditionalLogLikelihood vzxmp mlkl
     in (ll,pst)

poissonMomentValidator
    :: forall f n k x .
        ( ExponentialFamily x, KnownNat n, KnownNat k
        , Map Natural f (Mixture (Neurons n) k) x )
    => SampleMap (Neurons n) x -- ^ Validation Samples
    -> Natural # f (Mixture (Neurons n) k) x
    -> (Double,Double,Double,Double)
poissonMomentValidator vzxmp mlkl =
    let xs = M.keys vzxmp
        zss = M.elems vzxmp
        (ffs,crrs,skws,krts) = L.unzip4 $ do
            (zs,mx) <- zip zss $ mlkl >$>* xs
            let dtanrm,mxnrm :: Source # MultivariateNormal n
                dtanrm = mle $ S.map realToFrac <$> zs
                mxnrm = poissonMixtureCovariance mx
                dtacrrs =  listCorrelations $ multivariateNormalCorrelations dtanrm
                mxcrrs = listCorrelations $ multivariateNormalCorrelations mxnrm
                dtaffs = multivariateNormalFanoFactors dtanrm
                mxffs = multivariateNormalFanoFactors mxnrm
                zs' = V.map fromIntegral . V.fromList <$> L.transpose (S.toList <$> zs)
                dtamus = STAT.mean <$> zs'
                dtaskws0 = STAT.skewness <$> zs'
                dtakrts0 = STAT.kurtosis <$> zs'
                dtaskws = zipWith (/) dtaskws0 $ (**(-1/2)) <$> dtamus
                dtakrts = zipWith (/) dtakrts0 $ (**(-1)) <$> dtamus
                mxskws = S.toList $ poissonMixtureSkewnesses mx
                mxkrts = S.toList $ poissonMixtureExcessKurtosises mx
            return ( filter filterFun $ zip dtaffs mxffs
                   , filter filterFun $ zip dtacrrs mxcrrs
                   , filter filterFun $ zip dtaskws mxskws
                   , filter filterFun $ zip dtakrts mxkrts)
         in ( rSquared' $ concat ffs
            , rSquared' $ concat crrs
            , rSquared' $ concat skws
            , rSquared' $ concat krts )

comMomentValidator
    :: forall f n k x .
        ( KnownNat n, ExponentialFamily x, KnownNat k, Map Natural f (CBMixture n k) x )
    => SampleMap (Neurons n) x -- ^ Validation Samples
    -> Natural # f (CBMixture n k) x
    -> (Double,Double,Double,Double)
comMomentValidator vzxmp mlkl =
    let xs = M.keys vzxmp
        zss = M.elems vzxmp
        (ffs,crrs,skws,krts) = L.unzip4 $ do
            (zs,mx) <- zip zss $ mlkl >$>* xs
            let dtanrm,mxnrm :: Source # MultivariateNormal n
                dtanrm = mle $ S.map realToFrac <$> zs
                mxnrm = comMixtureCovariance mx
                dtacrrs =  listCorrelations $ multivariateNormalCorrelations dtanrm
                mxcrrs = listCorrelations $ multivariateNormalCorrelations mxnrm
                dtaffs = multivariateNormalFanoFactors dtanrm
                mxffs = multivariateNormalFanoFactors mxnrm
                zs' = V.map fromIntegral . V.fromList <$> L.transpose (S.toList <$> zs)
                dtamus = STAT.mean <$> zs'
                dtaskws0 = STAT.skewness <$> zs'
                dtakrts0 = STAT.kurtosis <$> zs'
                dtaskws = zipWith (/) dtaskws0 $ (**(-1/2)) <$> dtamus
                dtakrts = zipWith (/) dtakrts0 $ (**(-1)) <$> dtamus
                mxskws = S.toList $ comMixtureSkewnesses mx
                mxkrts = S.toList $ comMixtureExcessKurtosises mx
            return ( filter filterFun $ zip dtaffs mxffs
                   , filter filterFun $ zip dtacrrs mxcrrs
                   , filter filterFun $ zip dtaskws mxskws
                   , filter filterFun $ zip dtakrts mxkrts)
         in ( rSquared' $ concat ffs
            , rSquared' $ concat crrs
            , rSquared' $ concat skws
            , rSquared' $ concat krts )


