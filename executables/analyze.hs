{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}

{-# LANGUAGE
    FlexibleContexts,
    TypeFamilies,
    TypeOperators,
    TypeApplications,
    ScopedTypeVariables,
    DataKinds
    #-}


--- Imports ---


import Goal.Core
import Goal.Geometry
import Goal.Probability
import Goal.Graphical hiding (FactorAnalysis)

import Core
import NeuralMixtures
import NeuralMixtures.VonMises
import NeuralMixtures.Discrete
import Gaussian

import qualified Goal.Core.Vector.Storable as S
import qualified Goal.Core.Vector.Boxed as B
import qualified Goal.Core.Vector.Generic as G
import qualified Data.List as L
import qualified Data.Map as M

--import qualified Statistics.Sample as STAT
--import qualified Data.Vector as V

--- Globals ---


-- Ploting --

-- Number of samples for continuous variables
vmsmps :: [Double]
vmsmps = take 100 $ range mnang mxang 101

-- Number of bins for histograms
nbns :: Int
nbns = 8

-- Basic file names
axscsv,tccsvnm,wdcsvnm,ffsctnm,ffsqrnm,ncmtxnm,ncsctnm,ncsqrnm,nchstnm,fshnm,lnfshnm :: String
axscsv = "stimulus-axis"
tccsvnm = "tuning-curves"
wdcsvnm = "weight-dependence"
ffsctnm = "fano-factor-scatter"
ffsqrnm = "fano-factor-rsquared"
ncmtxnm = "noise-correlation-matrix"
ncsctnm = "noise-correlation-scatter"
ncsqrnm = "noise-correlation-rsquared"
nchstnm = "noise-correlation-histogram"
--cphstnm = "com-parameter-histogram"
fshnm = "fisher-information"
lnfshnm = "linear-fisher-information"

-- File name manupulation
plotFile :: String -> FilePath
plotFile = ("plot-files/analyze/" ++)

frameFile :: String -> Int -> String
frameFile rt idx = rt ++ "-frame-" ++ show idx

appendDir :: String -> String -> String
appendDir drs dr = drs ++ "/" ++ dr

-- Util
roundK :: Int -> Double -> String
roundK k vl = showFFloat (Just k) vl ""

gaussianMLE
    :: KnownNat n
    => [Response n]
    -> Source # MultivariateNormal n
gaussianMLE = mle . map (S.map fromIntegral)

--- Main ---


main :: IO ()
main = do

    let prgstr = "Takes a directory (spaces instead of /), where a parameters.dat file should be located. It will then run a series of analyses on the given model and plot the results."
        hdrstr = "Analyze and plot statistics of a neural model and dataset."
        opts = info (allOpts <**> helper) (fullDesc <> progDesc prgstr <> header hdrstr)
    runOpts =<< execParser opts


--- CLI ---


data CorrelationOpts = CorrelationOpts [String] Bool

correlationOpts :: Parser CorrelationOpts
correlationOpts = CorrelationOpts
    <$> many (strArgument ( metavar "Model subdirectories (i.e. experiment dataset form class algorithm"))
    <*> switch
        ( short 'c'
        <> long "continuous-analysis"
        <> help "Interpret stimuli as continuous numbers" )

data AllOpts = AllOpts ExperimentOpts CorrelationOpts

allOpts :: Parser AllOpts
allOpts = AllOpts <$> experimentOpts <*> correlationOpts

runOpts :: AllOpts -> IO ()
runOpts ( AllOpts(ExperimentOpts expmnt dst) (CorrelationOpts sbdrs fshbl)) = do

    let ldpth = analysisPath expmnt dst sbdrs

    (n,k,cs) <- readLatentVariableModel ldpth
    (_,s,zxs0) <- readNeuralData expmnt dst

    putStrLn "\nNumber of Neurons:"
    print n

    putStrLn "\nNumber of Latents:"
    print k

    putStrLn "\nNumber of Stimuli:"
    print s

    putStrLn "\nNumber of Trials:"
    print $ length zxs0

    case (someNatVal n, someNatVal k, someNatVal $ s-1)

        of ( SomeNat (Proxy :: Proxy n)
               , SomeNat (Proxy :: Proxy k)
               , SomeNat (Proxy :: Proxy s) ) -> do

            let zxs = strengthenNeuralData zxs0
                zxmp = conditionalDataMap zxs

            case (dst,sbdrs) of

                ("poisson",["true"]) -> do
                    let lkl :: Natural # IPCM n k VonMises
                        lkl = Point . fromJust $ S.fromList cs
                    poissonVonMisesAnalysis ldpth zxmp lkl

                ("com-based",["true"]) -> do
                    let lkl :: Natural # CBCM n k VonMises
                        lkl = Point . fromJust $ S.fromList cs
                    comVonMisesAnalysis ldpth zxmp lkl

                (_,["von-mises","poisson"]) -> do
                    let lkl :: Natural # IPCM n k VonMises
                        lkl = Point . fromJust $ S.fromList cs
                    poissonVonMisesAnalysis ldpth zxmp lkl

                (_,["von-mises","com-based"]) -> do
                    let lkl :: Natural # CBCM n k VonMises
                        lkl = Point . fromJust $ S.fromList cs
                    comVonMisesAnalysis ldpth zxmp lkl

                (_,["discrete","poisson"]) -> do
                    let lkl :: Natural # IPCM n k (Categorical s)
                        lkl = Point . fromJust $ S.fromList cs
                    poissonDiscreteAnalysis ldpth fshbl zxmp lkl

                (_,["maximal","poisson"]) -> do
                    let lkl :: Natural # Mixture (Neurons n) k <* Categorical s
                        lkl = Point . fromJust $ S.fromList cs
                    poissonDiscreteAnalysis ldpth fshbl zxmp lkl

                (_,["maximal","com-based"]) -> do
                    let lkl :: Natural # CBMixture n k <* Categorical s
                        lkl = Point . fromJust $ S.fromList cs
                    comDiscreteAnalysis ldpth fshbl zxmp lkl

                (_,["discrete","com-based"]) -> do
                    let lkl :: Natural # CBCM n k (Categorical s)
                        lkl = Point . fromJust $ S.fromList cs
                    comDiscreteAnalysis ldpth fshbl zxmp lkl

                (_,["factor-analysis"]) -> do
                    let lkl :: B.Vector (s+1) (Source # FactorAnalysis n k)
                        lkl = Point . G.convert <$>
                            (B.breakEvery . fromJust $ B.fromList cs)
                    factorAnalysisAnalysis ldpth fshbl zxmp lkl

                _ -> error "Unrecognized directory structure"


--- CLI Functions ---


poissonVonMisesAnalysis
    :: (KnownNat n, KnownNat k)
    => String -- Load path
    -> M.Map String [Response n] -- Data Covariances
    -> Natural # IPCM n k VonMises -- Model
    -> IO ()
poissonVonMisesAnalysis ldpth zxmp lkl = do

    let vmmp = toCircularDatamap zxmp
    let mdlcvrs = poissonMixtureCovariance <$> lkl >$>* M.keys vmmp

        dtacvrs = M.elems $ gaussianMLE <$> zxmp
        xs' = M.keys vmmp
        nrmxmp = M.fromList . zip xs' $ poissonMixtureCovariance <$> lkl >$>* xs'

    goalExport ldpth "stimulus-labels" $ Only . roundK 2 <$> xs'
    poissonVonMisesTuning ldpth lkl
    compareSecondOrder ldpth dtacvrs mdlcvrs
    compareLinearFisher ldpth vmmp nrmxmp

comVonMisesAnalysis
    :: (KnownNat n, KnownNat k)
    => String -- Load path
    -> M.Map String [Response n] -- Data Covariances
    -> Natural # CBCM n k VonMises -- Model
    -> IO ()
comVonMisesAnalysis ldpth zxmp lkl = do

    let vmmp = toCircularDatamap zxmp
    let mdlcvrs = comMixtureCovariance <$> lkl >$>* M.keys vmmp

    let dtacvrs = M.elems $ gaussianMLE <$> zxmp
        xs' = M.keys vmmp
        nrmxmp = M.fromList . zip xs' $ comMixtureCovariance <$> lkl >$>* xs'

    goalExport ldpth "stimulus-labels" $ Only . roundK 2 <$> xs'
    comBasedVonMisesTuning ldpth lkl
    compareSecondOrder ldpth dtacvrs mdlcvrs
    --comBasedHistogram ldpth mxmdls
    compareLinearFisher ldpth vmmp nrmxmp

poissonDiscreteAnalysis
    :: forall f n k s
        . (Map Natural f (Mixture (Neurons n) k) (Categorical s)
          , KnownNat n, KnownNat k, KnownNat s )
    => FilePath -- Load path
    -> Bool
    -> M.Map String [Response n] -- Data Covariances
    -> Natural # f (Mixture (Neurons n) k) (Categorical s) -- Model
    -> IO ()
poissonDiscreteAnalysis ldpth fshbl zxmp lkl = do

    let (zctmp, xctmp) = toDiscreteDatamap zxmp
        mxmdls = lkl >$>* M.keys zctmp
        dtacvrs = M.elems $ gaussianMLE <$> zctmp
        mdlcvrs = poissonMixtureCovariance <$> mxmdls

    poissonDiscreteTuning ldpth xctmp lkl

    --let mdlskwss = S.toList . poissonMixtureSkewnesses <$> mxmdls
    --    mdlkrtss = S.toList . poissonMixtureExcessKurtosises <$> mxmdls

    --compareHigherOrder ldpth zxmp mdlskwss mdlkrtss
    goalExport ldpth "stimulus-labels" $ Only <$> M.keys zxmp

    when fshbl $ do

        let zxmp' = M.mapKeys read zxmp
        compareLinearFisher ldpth zxmp' . M.fromList $ zip (M.keys zxmp') mdlcvrs
        goalExport ldpth "stimulus-labels" $ Only . roundK 2 <$> M.keys zxmp'

    compareSecondOrder ldpth dtacvrs mdlcvrs

comDiscreteAnalysis
    :: forall f n k s
        . (Map Natural f (CBMixture n k) (Categorical s)
          , KnownNat n, KnownNat k, KnownNat s )
    => FilePath -- Load path
    -> Bool
    -> M.Map String [Response n] -- Data Covariances
    -> Natural # f (CBMixture n k) (Categorical s) -- Model
    -> IO ()
comDiscreteAnalysis ldpth fshbl zxmp lkl = do

    let (zctmp, xctmp) = toDiscreteDatamap zxmp
        mxmdls = lkl >$>* M.keys zctmp
        dtacvrs = M.elems $ gaussianMLE <$> zctmp
        mdlcvrs = comMixtureCovariance <$> mxmdls

    comBasedDiscreteTuning ldpth xctmp lkl
    --comBasedHistogram ldpth 100 mxmdls

    --let mdlskwss = S.toList . comMixtureSkewnesses <$> mxmdls
    --    mdlkrtss = S.toList . comMixtureExcessKurtosises <$> mxmdls

    --compareHigherOrder ldpth zxmp mdlskwss mdlkrtss
    goalExport ldpth "stimulus-labels" $ Only <$> M.keys zxmp

    when fshbl $ do

        let zxmp' = M.mapKeys read zxmp
        compareLinearFisher ldpth zxmp' . M.fromList $ zip (M.keys zxmp') mdlcvrs
        goalExport ldpth "stimulus-labels" $ Only . roundK 2 <$> M.keys zxmp'

    compareSecondOrder ldpth dtacvrs mdlcvrs

factorAnalysisAnalysis
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => FilePath -- Load path
    -> Bool
    -> M.Map String [Response n] -- Data Covariances
    -> B.Vector s (Source # FactorAnalysis n k) -- Model
    -> IO ()
factorAnalysisAnalysis ldpth fshbl zxmp lkl = do

    let dtacvrs = M.elems $ gaussianMLE <$> zxmp
        mdlcvrs = toMultivariateNormal <$> B.toList lkl

    when fshbl $ do

            let zxmp' = M.mapKeys read zxmp
            compareLinearFisher ldpth zxmp' . M.fromList $ zip (M.keys zxmp') mdlcvrs

    compareSecondOrder ldpth dtacvrs mdlcvrs



--- Plot Functions ---


-- Higher-Order Statistics --

compareSecondOrder
    :: KnownNat n
    => String
    -> [Source # MultivariateNormal n]
    -> [Source # MultivariateNormal n]
    -> IO ()
compareSecondOrder ldpth nrms1 nrms2 = do

    -- Analyze

    let nstps = length nrms1
        dly = 100 :: Int

    let crrss1 = multivariateNormalCorrelations <$> nrms1
        crrss2 = multivariateNormalCorrelations <$> nrms2
        ffss1 = multivariateNormalFanoFactors <$> nrms1
        ffss2 = multivariateNormalFanoFactors <$> nrms2
        lcrrss1 = listCorrelations <$> crrss1
        lcrrss2 = listCorrelations <$> crrss2

    let ffsss = concat ffss1 ++ concat ffss2
        ffmn = roundSD 2 $ minimum ffsss
        ffmx = roundSD 2 $ maximum ffsss

    let crrsss = concat lcrrss1 ++ concat lcrrss2
        crrmn0,crrmx0 :: Int
        crrmn0 = floor . (*10) $ minimum crrsss
        crrmx0 = ceiling . (*10) $ maximum crrsss
        crrmn = (*0.1) $ fromIntegral crrmn0
        crrmx = (*0.1) $ fromIntegral crrmx0
        [crr0,crr1] = take 2 $ range crrmn crrmx (nbns + 1)
        crrstp = crr1 - crr0

    -- Export

    (mxhsts,ffr2s,crrr2s) <- fmap unzip3 . sequence $ do

        (idx,crrs1,crrs2,lcrrs1,lcrrs2,ffs1,ffs2) <- L.zip7 [0 :: Int ..]
            crrss1 crrss2 lcrrss1 lcrrss2 ffss1 ffss2

        let mlticrrs =  S.combineTriangles (S.replicate 1) crrs2 crrs1
            (bns,hsts,_) = histograms nbns (Just (crrmn,crrmx)) [lcrrs1,lcrrs2]
            hststrs = L.transpose $ map (map show) hsts
            mxhst = maximum $ concat hsts
            bnstrs = roundK 2 <$> bns

        return $ do

            goalExport (appendDir ldpth ffsctnm) (frameFile ffsctnm idx) $ zip ffs1 ffs2
            goalExport (appendDir ldpth nchstnm) (frameFile nchstnm idx) $ zipWith (:) bnstrs hststrs
            goalExport (appendDir ldpth ffsctnm) (frameFile ffsctnm idx) $ zip ffs1 ffs2
            goalExport (appendDir ldpth nchstnm) (frameFile nchstnm idx) $ zipWith (:) bnstrs hststrs
            goalExport (appendDir ldpth ncmtxnm) (frameFile ncmtxnm idx)
                $  map (roundK 4) . S.toList <$> S.toList (S.toRows mlticrrs)
            goalExport (appendDir ldpth ncsctnm) (frameFile ncsctnm idx) $ L.zip lcrrs1 lcrrs2
            return (mxhst, roundK 3 . rSquared' $ zip ffs1 ffs2, roundK 3 . rSquared' $ zip lcrrs1 lcrrs2)

    -- Plot

    let prms = [("dly",show dly),("nstps",show nstps)]
        ffbnds = [("ffmn", roundK 2 ffmn),("ffmx", roundK 2 ffmx)]
        ncbnds = [("crrmn", roundK 2 crrmn),("crrmx", roundK 2 crrmx)]
        mxhst' :: Int
        mxhst' = round $ 1.05 * (fromIntegral $ maximum mxhsts :: Double)
        nchstprms = prms ++ ncbnds ++
            [ ("mxhst", show mxhst'), ("crrmn",roundK 2 crrmn)
            , ("crrmx", roundK 2 crrmx), ("crrstp", roundK 3 crrstp)
            ]

    goalExport ldpth ffsqrnm $ Only <$> ffr2s
    goalExport ldpth ncsqrnm $ Only <$> crrr2s

    runGnuplotWithVariables ldpth (plotFile ffsctnm) $ prms ++ ffbnds
    runGnuplotWithVariables ldpth (plotFile nchstnm) nchstprms
    runGnuplotWithVariables ldpth (plotFile ncmtxnm) $ prms ++ ncbnds
    runGnuplotWithVariables ldpth (plotFile ncsctnm) $ prms ++ ncbnds

compareLinearFisher
    :: KnownNat n
    => FilePath
    -> M.Map Double [S.Vector n Int]
    -> M.Map Double (Source # MultivariateNormal n)
    -> IO ()
compareLinearFisher ldpth zxmp mdlxmp = do

    let (lcs,_,empfshs) = unzip3 $ empiricalLinearFisherInformations zxmp
        mdlfshs = map snd $ linearFisherInformation mdlxmp

    goalExport ldpth lnfshnm $ zip3 lcs empfshs mdlfshs
    runGnuplot ldpth (plotFile fshnm)

--compareHigherOrder
--    :: KnownNat n
--    => FilePath
--    -> M.Map String [Response n]
--    -> [[Double]]
--    -> [[Double]]
--    -> IO ()
--compareHigherOrder ldpth zxmp mdlskwss mdlkrtss = do
--
--    let (dtaskwss,dtakrtss) = unzip $ do
--
--            zs <- M.elems zxmp
--            let zs' = V.map fromIntegral . V.fromList <$> L.transpose (S.toList <$> zs)
--                sks = STAT.skewness <$> zs'
--                krts = STAT.kurtosis <$> zs'
--            return (sks,krts)
--
--
--    crrss <- sequence $ do
--
--        (idx,dtaskws,mdlskws,dtakrts,mdlkrts)
--            <- L.zip5 [0 :: Int ..] dtaskwss mdlskwss dtakrtss mdlkrtss
--
--        let dtamdlskws = L.zip dtaskws mdlskws
--            dtamdlkrts = L.zip dtakrts mdlkrts
--
--        return $ do
--
--            goalExport ldpth (frameFile "skewness-scatter" idx) dtamdlskws
--            goalExport ldpth (frameFile "kurtosis-scatter" idx) dtamdlkrts
--            return (rSquared' dtamdlskws, rSquared' dtamdlkrts)
--
--    let (skwcrrs,krtcrrs) = unzip crrss
--
--    goalExport ldpth "skewness-rsquared" $ Only <$> skwcrrs
--    goalExport ldpth "kurtosis-rsquared" $ Only <$> krtcrrs

--comBasedHistogram
--    :: forall n k . ( KnownNat n, KnownNat k )
--    => String
--    -> Int
--    -> [Natural # CBMixture n k]
--    -> IO ()
--comBasedHistogram ldpth dly mxmdls = do
--
--    let nstps = length mxmdls
--
--    sequence_ $ do
--
--        (idx,mxmdl) <- zip [0 :: Int ..] mxmdls
--
--        let cms = listCoordinates . fst $ splitCBMixture mxmdl
--            (bns,hsts,_) = histograms nbns Nothing [cms]
--            hsts' = L.transpose $ map (map fromIntegral) hsts
--            bns' = roundSD 2 <$> bns
--
--        return $ do
--
--            goalExport (appendDir ldpth cphstnm) (frameFile cphstnm idx) $ zipWith (:) bns' hsts'
--
--    let prms = [("dly",show dly),("nstps",show nstps)]
--    runGnuplotWithVariables ldpth (plotFile cphstnm) prms

-- Tuning Curves --

poissonVonMisesTuning
    :: ( KnownNat k, KnownNat n )
    => String -- ^ Load Path
    -> Natural # IPCM n k VonMises -- Model
    -> IO ()
poissonVonMisesTuning ldpth lkl = do

    let (tcss,wghtss) = unzip $ do
                nhrm <- lkl >$>* vmsmps
                let (nrns,_,cts) = splitHarmonium $ toMean nhrm
                    wghts = S.toList $ categoricalWeights cts
                    tcs = listCoordinates nrns
                return ( tcs, wghts )

    analyzeCurves False ldpth vmsmps tcss wghtss

    let fshs = poissonFisherInformation lkl <$> vmsmps

    goalExport ldpth fshnm $ zip vmsmps fshs

comBasedVonMisesTuning
    :: ( KnownNat k, KnownNat n )
    => String -- ^ Load Path
    -> Natural # CBCM n k VonMises -- Model
    -> IO ()
comBasedVonMisesTuning ldpth lkl = do

    let (tcss,ctss) = unzip $ do
                nhrm <- lkl >$>* vmsmps
                let (nrns,_,cts) = splitHarmonium $ toMean nhrm
                    wghts = S.toList $ categoricalWeights cts
                    tcs = listCoordinates $ mapReplicatedPoint (fst . split) nrns
                return ( tcs, wghts )

    analyzeCurves False ldpth vmsmps tcss ctss

    let fshs = comFisherInformation lkl <$> vmsmps

    goalExport ldpth fshnm $ zip vmsmps fshs


poissonDiscreteTuning
    :: forall f k n s
        . ( Map Natural f (Mixture (Neurons n) k) (Categorical s)
          , KnownNat n, KnownNat k, KnownNat s )
    => String -- ^ Load Path
    -> M.Map Int String -- ^ Stimulus Map
    -> Natural # f (Mixture (Neurons n) k) (Categorical s) -- Model
    -> IO ()
poissonDiscreteTuning ldpth ctxmp lkl = do

    let (idxs,xsmps) = unzip $ M.toList ctxmp
    let (tcss,ctss) = unzip $ do
                nhrm <- lkl >$>* idxs
                let (nrns,_,cts) = splitHarmonium $ toMean nhrm
                    wghts = S.toList $ categoricalWeights cts
                    tcs = listCoordinates nrns
                return ( tcs, wghts )

    analyzeCurves True ldpth xsmps tcss ctss

comBasedDiscreteTuning
    :: forall f k n s
        . ( Map Natural f (CBMixture n k) (Categorical s)
          , KnownNat n, KnownNat k, KnownNat s )
    => String -- ^ Load Path
    -> M.Map Int String -- ^ Stimulus Map
    -> Natural # f (CBMixture n k) (Categorical s) -- Model
    -> IO ()
comBasedDiscreteTuning ldpth ctxmp lkl = do

    let (idxs,xsmps) = unzip $ M.toList ctxmp
    let (tcss,ctss) = unzip $ do
                nhrm <- lkl >$>* idxs
                let (nrns,_,cts) = splitHarmonium $ toMean nhrm
                    wghts = S.toList $ categoricalWeights cts
                    tcs = listCoordinates $ mapReplicatedPoint (fst . split) nrns
                return ( tcs,  wghts )

    analyzeCurves True ldpth xsmps tcss ctss

analyzeCurves :: ToField x =>
    Bool -> String -> [x] -> [[Double]] -> [[Double]] -> IO ()
analyzeCurves dsc ldpth stms tcss ctss = do

    goalExport ldpth axscsv (Only <$> stms)
    goalExport ldpth tccsvnm tcss
    goalExport ldpth wdcsvnm ctss

    let tcpltnm = if dsc then "discrete-tuning" else "continuous-tuning"

    runGnuplot ldpth $ plotFile tcpltnm
