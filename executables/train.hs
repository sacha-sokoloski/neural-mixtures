#! /usr/bin/env stack
-- stack runghc --

{-# OPTIONS_GHC -fplugin=GHC.TypeLits.KnownNat.Solver -fplugin=GHC.TypeLits.Normalise -fconstraint-solver-iterations=10 #-}

{-# LANGUAGE
    FlexibleContexts,
    TypeFamilies,
    TypeOperators,
    TypeApplications,
    ScopedTypeVariables,
    DataKinds
    #-}


--- Imports ---


import Core
import NeuralMixtures
import NeuralMixtures.VonMises
import NeuralMixtures.Discrete
import Gaussian

import Goal.Core
import Goal.Geometry
import Goal.Probability
import Goal.Graphical hiding (FactorAnalysis)

import qualified Goal.Core.Vector.Boxed as B
import qualified Data.Map.Strict as M


--- Globals ---

llnm,llgpi :: String
llnm = "log-likelihood-ascent"
llgpi = "plot-files/train/" ++ llnm


--- IO ---


-- Main --

main :: IO ()
main = do

    let prgstr = "Train a model and save the results for further analysis."
        hdrstr = "Fit a variety of CMs, as well as factor analysis to a specified dataset. Numerous learning parameters are available to maximize performance."
        opts = info (allOpts <**> helper) (fullDesc <> progDesc prgstr <> header hdrstr)
    runOpts =<< execParser opts

-- CLI --

data TrainingOpts = TrainingOpts [String] NatNumber Int Double Double Int Double Double

trainingOpts :: Parser TrainingOpts
trainingOpts = TrainingOpts
    <$> many (strArgument
        ( metavar "Model subdirectories (i.e. {von-mises,discrete,maximal,factor-analysis} {poisson,com-based}") )
    <*> option auto
        ( short 'k'
        <> long "k-components"
        <> help "Number of components."
        <> showDefault
        <> value 7 )
    <*> option auto
        ( short 'e'
        <> long "n-epochs"
        <> help "Number of epochs to run the learning over."
        <> showDefault
        <> value 200 )
    <*> option auto
        ( short 'l'
        <> long "max-learning-rate"
        <> help "The maximum learning rate."
        <> showDefault
        <> value 3e-3 )
    <*> option auto
        ( short 'L'
        <> long "min-learning-rate"
        <> help "The minum learning rate."
        <> showDefault
        <> value 1e-3 )
    <*> option auto
        ( short 's'
        <> long "n-steps"
        <> help "Number of steps (for gradient-ascent on maximization step)."
        <> showDefault
        <> value 50 )
    <*> option auto
        ( short 'r'
        <> long "pretraining-size"
        <> help "Percentage (0-1) of training to iterations to pretrain CoM models."
        <> showDefault
        <> value 0.8 )
    <*> option auto
        ( short 'v'
        <> long "validation-size"
        <> help "Percentage (0-1) of the data to hold-out for validation. 0 causes the training set to be used as the validation set."
        <> showDefault
        <> value 0 )

data AllOpts = AllOpts ExperimentOpts TrainingOpts

allOpts :: Parser AllOpts
allOpts = AllOpts <$> experimentOpts <*> trainingOpts

splitter :: Double -> [x] -> ([x],[x])
splitter 0 xs = (xs,xs)
splitter vsz xs =
    let vn = round $ vsz * fromIntegral (length xs)
     in splitAt vn xs

runOpts :: AllOpts -> IO ()
runOpts ( AllOpts (ExperimentOpts expmnt dst)
    (TrainingOpts sbdrs k nepchs mxeps mneps nstps cfrc vprcnt) ) = do

    let ldpth = analysisPath expmnt dst sbdrs

    (n,s,zxs0) <- readNeuralData expmnt dst

    putStrLn "\nNumber of Neurons:"
    print n

    putStrLn "\nNumber of Stimuli:"
    print s

    putStrLn "\nNumber of Trials:"
    print $ length zxs0

    case (someNatVal n, someNatVal k, someNatVal $ s-1)

        of ( SomeNat prxn, SomeNat prxk, SomeNat prxs ) -> do

            let zxmp = conditionalDataMap $ strengthenNeuralData zxs0

            let tvzxmp = splitter vprcnt <$> zxmp
                vzxmp = fst <$> tvzxmp
                tzxmp = snd <$> tvzxmp

            case sbdrs of

                ["von-mises",frm] ->
                    trainVonMises prxn prxk ldpth frm nepchs cfrc mxeps mneps nstps tzxmp vzxmp

                ["discrete",frm] ->
                    trainDiscrete prxn prxk prxs ldpth frm nepchs cfrc mxeps mneps nstps tzxmp vzxmp

                ["maximal",frm] ->
                    trainMaximal prxn prxk prxs ldpth frm nepchs cfrc mxeps mneps nstps tzxmp vzxmp

                ["factor-analysis"] -> do
                    trainFactorAnalysis prxn prxk prxs ldpth nepchs tzxmp vzxmp

                _ -> error "Unrecognized directory structure"

trainMaximal
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => Proxy n
    -> Proxy k
    -> Proxy s
    -> String
    -> String
    -> Int
    -> Double
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> IO ()
trainMaximal _ _ _ ldpth frm nepchs cfrc mxeps mneps nstps tzxmp0 vzxmp0 = do

    let tzxmp = fst $ toDiscreteDatamap tzxmp0
        vzxmp = fst $ toDiscreteDatamap vzxmp0

    let epss = epsilonRange mxeps mneps nepchs

    mlkl0 :: Natural # Mixture (Neurons n) k <*  Categorical s
        <- realize $ dataInitializeMaximalCM (-0.01,0.01) tzxmp

    case frm of

        "poisson" -> do

            let validator1 mlkl =
                    mapConditionalLogLikelihood vzxmp mlkl

            let (mlkl,ascnts) =
                    fitIPCM epss nstps tzxmp validator1 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts

            goalExport ldpth llnm $ Only <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mlkl

        "com-based" -> do

            let validator1 = mapConditionalLogLikelihood vzxmp
                validator2 = mapConditionalLogLikelihood vzxmp

            let (mxmlkl,ascnts) = fitMaximalCBCM
                    cfrc epss nstps tzxmp validator1 validator2 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts

            goalExport ldpth llnm $ (:[]) <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mxmlkl

        _ -> error "Form not supported"

trainDiscrete
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => Proxy n
    -> Proxy k
    -> Proxy s
    -> String
    -> String
    -> Int
    -> Double
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> IO ()
trainDiscrete _ _ _ ldpth frm nepchs cfrc mxeps mneps nstps tzxmp0 vzxmp0 = do

    let tzxmp = fst $ toDiscreteDatamap tzxmp0
        vzxmp = fst $ toDiscreteDatamap vzxmp0

    let epss = epsilonRange mxeps mneps nepchs

    mlkl0 :: Natural # IPCM n k (Categorical s)
        <- realize $ dataInitializeMinimalCM (-0.1,0.1) tzxmp

    case frm of

        "poisson" -> do

            let validator1 mlkl =
                    mapConditionalLogLikelihood vzxmp mlkl

            let (mlkl,ascnts) =
                    fitIPCM epss nstps tzxmp validator1 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts


            goalExport ldpth llnm $ Only <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mlkl

        "com-based" -> do

            let validator1 = mapConditionalLogLikelihood vzxmp
                validator2 = mapConditionalLogLikelihood vzxmp

            let (mxmlkl,ascnts) = fitCBCM
                    cfrc epss nstps tzxmp validator1 validator2 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts

            goalExport ldpth llnm $ (:[]) <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mxmlkl

        _ -> error "Form not supported"

trainVonMises
    :: forall n k . (KnownNat n, KnownNat k)
    => Proxy n
    -> Proxy k
    -> String
    -> String
    -> Int
    -> Double
    -> Double
    -> Double
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> IO ()
trainVonMises _ _ ldpth frm nepchs cfrc mxeps mneps nstps tzxmp0 vzxmp0 = do

    let tzxmp = toCircularDatamap tzxmp0
        vzxmp = toCircularDatamap vzxmp0

    let epss = epsilonRange mxeps mneps nepchs

    let lkl0 = fitVonMisesIndependent 0.05 500 vzxmp

    let mlkl0 :: Natural # IPCM n k VonMises
        mlkl0 = sinusoidalModulations 0.1 0 lkl0

    case frm of

        "poisson" -> do

            let validator1 mlkl =
                    mapConditionalLogLikelihood vzxmp mlkl

            let (mlkl,ascnts) =
                    fitIPCM epss nstps tzxmp validator1 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts


            goalExport ldpth llnm $ Only <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mlkl

        "com-based" -> do

            let validator1 = mapConditionalLogLikelihood vzxmp
                validator2 = mapConditionalLogLikelihood vzxmp

            let (mxmlkl,ascnts) = fitCBCM
                    cfrc epss nstps tzxmp validator1 validator2 mlkl0

            sequence_ $ print <$> zip [0 :: Int ..] ascnts

            goalExport ldpth llnm $ (:[]) <$> ascnts
            runGnuplot ldpth llgpi

            writeCM ldpth mxmlkl

        _ -> error "Form not supported"

trainFactorAnalysis
    :: forall n k s . (KnownNat n, KnownNat k, KnownNat s)
    => Proxy n
    -> Proxy k
    -> Proxy s
    -> String
    -> Int
    -> M.Map String [Response n]
    -> M.Map String [Response n]
    -> IO ()
trainFactorAnalysis _ _ _ ldpth nepchs tzxmp0 vzxmp0 = do

    let tzxmp = fst $ toDiscreteDatamap tzxmp0
        vzxmp = fst $ toDiscreteDatamap vzxmp0

    let xss :: B.Vector (s+1) [Response n]
        xss = fromJust . B.fromList $ M.elems tzxmp
    fas0 :: B.Vector (s+1) (Source # FactorAnalysis n k)
        <- realize $ initializeConditionalFactorAnalysis xss

    let validator fas = average
            $ zipWith factorAnalysisValidator (M.elems vzxmp) $ B.toList fas

    let (fas,ascnts) = fitConditionalFactorAnalysis nepchs tzxmp validator fas0

    goalExport ldpth llnm $ Only <$> ascnts
    runGnuplot ldpth llgpi
    writeFactorAnalysis ldpth fas
