# Neural Mixtures

`neural-mixtures` is a library for working with conditional mixture models and applying them to neural data. Our paper on conditional mixtures can be found [here](https://elifesciences.org/articles/64615). A comprehensive tutorial on installing the `neural-mixtures` CLI and applying it to new datasets is available [here](https://sacha-sokoloski.gitlab.io/website/posts/papers/01-neural-mixtures.html).

Interested users are welcome to contact me for help understanding and applying my code. Moreover, I would be open to developing bindings for other languages if desired.
